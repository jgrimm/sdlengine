/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <unistd.h>

#include "Surface.h"
#include "Tile.h"
#include "Field.h"
#include "Renderer.h"
#include "Camera.h"
#include "GameManager.h"

int main(int argc, char** argv) {
    //Заглушка для Warning'а. Параметры argc, argv не используются намеренно
    argc = argc;
    argv = argv;
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cout << "Unable to init SDL: " << SDL_GetError() << std::endl;
        return -1;
    }

    atexit(SDL_Quit);
    SDL_WM_SetCaption("Project Khadgar", "Khadgar");

    std::shared_ptr<Camera> camera(new Camera(780, 626, 32, SDL_HWSURFACE | SDL_DOUBLEBUF));
    std::shared_ptr<GameManager> empty;
    Renderer renderer(camera, empty);

    std::shared_ptr<Surface> link(new Surface("link.png"));
//     link->setColorKey(0xFF00FF);

    SDL_Rect crop = {0, 0, 32, 32};
    std::shared_ptr<Surface> grassSurf(new Surface("tilea2.png", crop));

    int row = 30, col = 30;
    std::vector<std::vector<std::shared_ptr<Tile> > > map;

    for (int x = 0; x < row; ++x) {
        std::vector<std::shared_ptr<Tile> > rows;

        for (int y = 0; y < col; ++y) {
            std::shared_ptr<Tile> grassTile(new Tile(grassSurf));
            rows.push_back(grassTile);
        }

        map.push_back(rows);
    }

    std::shared_ptr<Field> field(new Field(map, map[0][0]->getSize()));

    renderer.drawField(field.get());
    Point coord(0, 0);
    renderer.drawSurface(link.get(), coord);
    renderer.updateScreen();

//  SDL_Flip(scrn.get());

    int done = 0;
    SDL_Event event;

    Uint8 transperent = 100;
    bool fade = true;
    link->setAlpha(transperent);
    
    while (done == 0) {
//         SDL_WaitEvent(&event);

     SDL_PollEvent(&event);
        if (event.type == SDL_QUIT) {
            done = 1;
        }

        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                done = 1;
            }

            if (event.key.keysym.sym == SDLK_LEFT) {
                Point cameraPos = camera->getPosition();
                cameraPos.x -= 5;
                camera->setPosition(cameraPos);
            }

            if (event.key.keysym.sym == SDLK_RIGHT) {
                Point cameraPos = camera->getPosition();
                cameraPos.x += 5;
                camera->setPosition(cameraPos);
            }

            if (event.key.keysym.sym == SDLK_UP) {
                Point cameraPos = camera->getPosition();
                cameraPos.y -= 5;
                camera->setPosition(cameraPos);
            }

            if (event.key.keysym.sym == SDLK_DOWN) {
                Point cameraPos = camera->getPosition();
                cameraPos.y += 5;
                camera->setPosition(cameraPos);
            }
        }

        usleep(6666);
        if (fade) {
            if (transperent < 255) {
                transperent += 1;
            } else {
                fade = !fade;
            }
        } else {
            if (transperent > 0) {
                transperent -= 1;
            } else {
                fade = !fade;
            }
        }
        link->setAlpha(transperent);
        
        renderer.cleanScreen();
        renderer.drawField(field.get());
        renderer.drawSurface(link.get(), coord);
        renderer.updateScreen();
    }

    exit(0);
}
