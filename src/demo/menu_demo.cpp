/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит демо для демонстрации возможностей движка по отрисовке 
 * меню
 */

#include <iostream>
#include <memory>
#include <SDL.h>
#include <unistd.h>

#include <sys/time.h>

#include "Application.h"
#include "GameManager.h"
#include "FSM.h"
#include "MenuState.h"
#include "GameState.h"
#include "TimersPool.h"

/**
 * @brief Точка входа программы
 *
 * @param argc количество аргументов командной строки
 * @param argv массив аргументов
 * @return int
 **/
int main(int argc, char** argv) {
    //Заглушка для Warning'а. Параметры argc, argv не используются намеренно
    argc = argc;
    argv = argv;
    
    timeval game_tv = {0, 6666};
    timeval render_tv = {0, 16666};
    
    Application& app = Application::getSingleton();
    app.onInit(780, 600, 32, SDL_HWSURFACE | SDL_DOUBLEBUF, game_tv, render_tv);
    
    std::shared_ptr<GameManager> gameManager = app.getGameManager();
    std::shared_ptr<FSM> fsm(new FSM());
    gameManager->setFSM(fsm);
    
    std::shared_ptr<State> menuState(new MenuState(fsm));
    menuState->onLoad();
    std::shared_ptr<State> gameState(new GameState(fsm));
    gameState->onLoad();
    fsm->pushState(menuState);
    fsm->pushState(gameState);
    
    timeval period = {0, 10000};
    TimersPool::getSingleton().setCheckPeriod(period);
    
    app.run();
    
    return 0;
}
