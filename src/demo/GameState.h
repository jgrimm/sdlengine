/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса состояния запущеной игры
 */

#ifndef GAME_STATE_H
#define GAME_STATE_H

#include "State.h"

/**
 * @brief Класс состояния запущеной игры
 **/
class GameState : public State {
public:
    GameState(std::shared_ptr<FSM> fsm);
    virtual ~GameState();
    
    virtual void onEnter();
    virtual void onLeave();
    virtual void onLoad();
    virtual void onEvent(SDL_Event& event);
    virtual void onTimer();

private:
    bool m_wasInit;
};

#endif // GAME_STATE_H
