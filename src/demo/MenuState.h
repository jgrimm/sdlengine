/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса состояния главного меню
 */

#ifndef MENU_STATE_H
#define MENU_STATE_H

#include "State.h"

class FSM;
class Timer;

/**
 * @brief Класс состояния "главное меню"
 **/
class MenuState : public State {
public:
    MenuState(std::shared_ptr<FSM> fsm);
    virtual ~MenuState();
    
    virtual void onEnter();
    virtual void onLeave();
    virtual void onLoad();
    virtual void onEvent(SDL_Event& event);
    virtual void onTimer();
    
    void think();
private:
    std::shared_ptr<Timer> m_thinkTimer;        //!< Таймер внутренней логики
};

#endif // MENU_STATE_H
