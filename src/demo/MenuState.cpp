/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса состояния главного меню
 */

#include <functional>
#include <SDL_events.h>

#include "MenuState.h"
#include "Surface.h"
#include "Background.h"
#include "SurfaceSet.h"
#include "AnimatedObject.h"
#include "Animation.h"
#include "Tile.h"
#include "Timer.h"
#include "TimersPool.h"
#include "Application.h"
#include "GameManager.h"
#include "FSM.h"

/**
 * @brief Конструктор
 *
 * @param fsm указатель на автомат состояний
 **/
MenuState::MenuState(std::shared_ptr<FSM> fsm) :
    State(fsm) {
}

/**
 * @brief Деструктор
 *
 **/
MenuState::~MenuState() {
}

/**
 * @brief Входа в состояние
 *
 * @return void
 **/
void MenuState::onEnter() {
    timeval tv = {0, 100000};
    std::shared_ptr<Timer> thinkTimer(new Timer(tv));
    m_thinkTimer = thinkTimer;
    m_thinkTimer->setCallback(std::bind(&MenuState::think, this));
//     TimersPool::getSingleton().addTimer(thinkTimer);
}

/**
 * @brief Выхода из состояния
 *
 * @return void
 **/
void MenuState::onLeave() {
    if (m_thinkTimer.get() != nullptr) {
        m_thinkTimer->beginDelete();
    }
}

/**
 * @brief Загрузка ресурсов
 *
 * @return void
 **/
void MenuState::onLoad() {
    std::shared_ptr<Surface> surface1(new Surface("MainBackground.png"));
    std::shared_ptr<Background> background(new Background(surface1));
    m_background = background;
    
    std::shared_ptr<Surface> surface2(new Surface("text3.png"));
    surface2->setColorKey(0xFF00FF);
    surface2->setAlpha(0);
    std::shared_ptr<Tile> text1(new Tile(surface2));
    Point pos1 = {30, 190};
    text1->setPosition(pos1);
    
    m_tiles.push_back(text1);    
}

/**
 * @brief Обработчик событий
 *
 * @param event событие
 * @return void
 **/
void MenuState::onEvent(SDL_Event& event) {
    event = event;
    if (event.type == SDL_KEYDOWN) {
        if (event.key.keysym.sym != SDLK_ESCAPE) {
            Application::getSingleton().getGameManager()->getFSM()->switchState(1);
        }
    }
}

/**
 * @brief Обработчик срабатывания таймера
 *
 * @return void
 **/
void MenuState::onTimer() {
    think();
}

/**
 * @brief Внутренняя логика
 *
 * @return void
 **/
void MenuState::think() {
    Uint8 alpha = m_tiles[0]->getSurface()->getAlpha();
    if (alpha + 3 < 255) {
        m_tiles[0]->getSurface()->setAlpha(alpha + 3);
    }
}
