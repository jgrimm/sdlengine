/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит демо для демонстрации возможностей движка по отрисовке 
 * анимированных объектов
 */

#include <iostream>
#include <memory>
#include <vector>
#include <SDL.h>
#include <SDL_image.h>
#include <unistd.h>

#include <sys/time.h>

#include "Surface.h"
#include "SurfaceSet.h"
#include "Animation.h"
#include "AnimatedObject.h"
#include "Renderer.h"
#include "Camera.h"
#include "GameManager.h"
#include "TimersPool.h"

/**
 * @brief Точка входа программы
 *
 * @param argc количество аргументов командной строки
 * @param argv массив аргументов
 * @return int
 **/
int main(int argc, char** argv) {
    //Заглушка для Warning'а. Параметры argc, argv не используются намеренно
    argc = argc;
    argv = argv;
    
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cout << "Unable to init SDL: " << SDL_GetError() << std::endl;
        return -1;
    }
    
    atexit(SDL_Quit);
    SDL_WM_SetCaption("Project Khadgar", "Khadgar");
    
    std::shared_ptr<Camera> camera(new Camera(1024, 768, 32, SDL_HWSURFACE | SDL_DOUBLEBUF));
    std::shared_ptr<GameManager> empty;
    Renderer renderer(camera, empty);
    
    const int width = 96 / 3;
    const int height = 128 / 4;
    SDL_Rect rect = {0, 0, width, height};
    
    std::shared_ptr<SurfaceSet> set1(new SurfaceSet());
    std::vector<timeval> times;
    timeval tv = {0, 200000};
    
    for (int w = 0; w < 3; ++w) {
        for (int h = 0; h < 1; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("guy.png", rect));
//             surface->setColorKey(0xFF00FF);
            set1->push(surface);
            times.push_back(tv);
        }
    }
    std::shared_ptr<Animation> animation1(new Animation());
    animation1->load(set1, times, ANIM_WALK_S);
    
    std::shared_ptr<SurfaceSet> set2(new SurfaceSet());
    for (int w = 0; w < 3; ++w) {
        for (int h = 1; h < 2; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("guy.png", rect));
            //             surface->setColorKey(0xFF00FF);
            set2->push(surface);
        }
    }
    std::shared_ptr<Animation> animation2(new Animation());
    animation2->load(set2, times, ANIM_WALK_W);
    
    std::shared_ptr<SurfaceSet> set3(new SurfaceSet());
    for (int w = 0; w < 3; ++w) {
        for (int h = 2; h < 3; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("guy.png", rect));
            //             surface->setColorKey(0xFF00FF);
            set3->push(surface);
        }
    }
    std::shared_ptr<Animation> animation3(new Animation());
    animation3->load(set3, times, ANIM_WALK_E);
    
    std::shared_ptr<SurfaceSet> set4(new SurfaceSet());
    for (int w = 0; w < 3; ++w) {
        for (int h = 3; h < 4; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("guy.png", rect));
            //             surface->setColorKey(0xFF00FF);
            set4->push(surface);
        }
    }
    std::shared_ptr<Animation> animation4(new Animation());
    animation4->load(set4, times, ANIM_WALK_N);
    
    std::shared_ptr<SurfaceSet> set5(new SurfaceSet());
    for (int w = 1; w < 2; ++w) {
        for (int h = 0; h < 1; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("guy.png", rect));
            //             surface->setColorKey(0xFF00FF);
            set5->push(surface);
        }
    }
    std::shared_ptr<Animation> animation5(new Animation());
    std::vector<timeval> times1;
    times1.push_back(tv);
    animation5->load(set5, times1, ANIM_STAND_S);
    
    std::shared_ptr<AnimatedObject> object(new AnimatedObject());
    object->addAnimation(animation1);
    object->addAnimation(animation2);
    object->addAnimation(animation3);
    object->addAnimation(animation4);
    object->addAnimation(animation5);
    object->runAnimation(ANIM_STAND_S, true);
    
    renderer.drawAnimatedObject(object);
    
    bool run = true;
    SDL_Event event;
    
    TimersPool::getSingleton().run();
    
    while (run) {
//         SDL_WaitEvent(&event);
        SDL_PollEvent(&event);
        if (event.type == SDL_QUIT) {
            run = false;
        }
        
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                run = false;
            }
            
            if (event.key.keysym.sym == SDLK_LEFT) {
                object->runAnimation(ANIM_WALK_E, true);
                Point cameraPos = camera->getPosition();
                cameraPos.x -= 3;
                camera->setPosition(cameraPos);
            }
            
            if (event.key.keysym.sym == SDLK_RIGHT) {
                object->runAnimation(ANIM_WALK_W, true);
                Point cameraPos = camera->getPosition();
                cameraPos.x += 3;
                camera->setPosition(cameraPos);
            }
            
            if (event.key.keysym.sym == SDLK_UP) {
                object->runAnimation(ANIM_WALK_S, true);
                Point cameraPos = camera->getPosition();
                cameraPos.y -= 3;
                camera->setPosition(cameraPos);
            }
            
            if (event.key.keysym.sym == SDLK_DOWN) {
                object->runAnimation(ANIM_WALK_N, true);
                Point cameraPos = camera->getPosition();
                cameraPos.y += 3;
                camera->setPosition(cameraPos);
            }
            
        }
        if (event.type == SDL_KEYUP) {
            object->runAnimation(ANIM_STAND_S, true);
        }
        renderer.cleanScreen();
        renderer.drawAnimatedObject(object);
        renderer.updateScreen();
        usleep(16666);
    }
    
    exit(0);
}
