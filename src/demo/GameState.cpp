/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл реализацию описание класса состояния запущеной игры
 */

#include <SDL.h>
#include <ctime>

#include "GameState.h"
#include "Surface.h"
#include "SurfaceSet.h"
#include "Animation.h"
#include "AnimatedObject.h"
#include "Background.h"
#include "FSM.h"
#include "Application.h"
#include "Tile.h"
#include "Field.h"
#include "Camera.h"

/**
 * @brief Конструктор
 *
 * @param fsm указатель на автомат состояний
 **/
GameState::GameState(std::shared_ptr<FSM> fsm) : 
    State(fsm),
    m_wasInit(false) {
}

/**
 * @brief Деструктор
 *
 **/
GameState::~GameState() {
}


/**
 * @brief Входа в состояние
 *
 * @return void
 **/
void GameState::onEnter() {

}

/**
 * @brief Выхода из состояния
 *
 * @return void
 **/
void GameState::onLeave() {

}

/**
 * @brief Загрузка ресурсов
 *
 * @return void
 **/
void GameState::onLoad() {
    srand(std::time(NULL));
    
    // FIXME решение на скорую руку. все переделать
    
    // Загрузка персонажа
    const int width = 32;
    const int height = 48;
    SDL_Rect rect = {0, 0, width, height};
    
    std::shared_ptr<SurfaceSet> set1(new SurfaceSet());
    std::vector<timeval> times;
    timeval tv = {0, 200000};
    
    for (int w = 3; w < 6; ++w) {
        for (int h = 0; h < 1; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("characters.png", rect));
            surface->setColorKey(0xff00ff);
            set1->push(surface);
            times.push_back(tv);
        }
    }
    std::shared_ptr<Animation> animation1(new Animation());
    animation1->load(set1, times, ANIM_WALK_S);
    
    std::shared_ptr<SurfaceSet> set2(new SurfaceSet());
    for (int w = 3; w < 6; ++w) {
        for (int h = 1; h < 2; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("characters.png", rect));
            surface->setColorKey(0xff00ff);
            set2->push(surface);
        }
    }
    std::shared_ptr<Animation> animation2(new Animation());
    animation2->load(set2, times, ANIM_WALK_W);
    
    std::shared_ptr<SurfaceSet> set3(new SurfaceSet());
    for (int w = 3; w < 6; ++w) {
        for (int h = 2; h < 3; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("characters.png", rect));
            surface->setColorKey(0xff00ff);
            set3->push(surface);
        }
    }
    std::shared_ptr<Animation> animation3(new Animation());
    animation3->load(set3, times, ANIM_WALK_E);
    
    std::shared_ptr<SurfaceSet> set4(new SurfaceSet());
    for (int w = 3; w < 6; ++w) {
        for (int h = 3; h < 4; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("characters.png", rect));
            surface->setColorKey(0xff00ff);
            set4->push(surface);
        }
    }
    std::shared_ptr<Animation> animation4(new Animation());
    animation4->load(set4, times, ANIM_WALK_N);
    
    std::shared_ptr<SurfaceSet> set5(new SurfaceSet());
    for (int w = 4; w < 5; ++w) {
        for (int h = 0; h < 1; ++h) {
            rect.x = w * width;
            rect.y = h * height;
            std::shared_ptr<Surface> surface(new Surface("characters.png", rect));
            surface->setColorKey(0xff00ff);
            set5->push(surface);
        }
    }
    std::shared_ptr<Animation> animation5(new Animation());
    std::vector<timeval> times1;
    times1.push_back(tv);
    animation5->load(set5, times1, ANIM_STAND_S);
    
    std::shared_ptr<AnimatedObject> object(new AnimatedObject());
    object->addAnimation(animation1);
    object->addAnimation(animation2);
    object->addAnimation(animation3);
    object->addAnimation(animation4);
    object->addAnimation(animation5);
    object->runAnimation(ANIM_STAND_S, true);
    
    std::shared_ptr<Camera> camera = Application::getSingleton().getCamera();
    Point pos = {camera->getWidth() / 2, camera->getHeight() / 2};
    object->setPosition(pos);
    
    m_objects.push_back(object);
    
//     Звездное небо
    std::shared_ptr<Surface> backSurf(new Surface("sky.png"));
    std::shared_ptr<Background> background(new Background(backSurf));
    m_background = background;
    
    // Поле
    SDL_Rect crop = {0, 0, 32, 32};
    std::shared_ptr<Surface> grassSurf(new Surface("tilea2.png", crop));
    
    int row = 30, col = 30;
    std::vector<std::vector<std::shared_ptr<Tile> > > map;
    
    for (int x = 0; x < row; ++x) {
        std::vector<std::shared_ptr<Tile> > rows;
        
        for (int y = 0; y < col; ++y) {
            std::shared_ptr<Tile> grassTile(new Tile(grassSurf));
            rows.push_back(grassTile);
        }
        
        map.push_back(rows);
    }
    
    std::shared_ptr<Field> field(new Field(map, map[0][0]->getSize()));
    
    SDL_Rect crop2 = {0, 96, 32, 32};
    std::shared_ptr<Surface> flowers1Surf(new Surface("tilea2.png", crop2));
    SDL_Rect crop3 = {32, 96, 32, 32};
    std::shared_ptr<Surface> flowers2Surf(new Surface("tilea2.png", crop3));
    SDL_Rect crop4 = {0, 192, 32, 32};
    std::shared_ptr<Surface> darkgras1Surf(new Surface("tilea2.png", crop4));
    SDL_Rect crop5 = {32, 192, 32, 32};
    std::shared_ptr<Surface> darkgras2Surf(new Surface("tilea2.png", crop5));
    
    int numTiles = rand() % 40 + 20;
    for (int i = 0; i < numTiles; ++i) {
        int x = rand() % field->getWidth();
        int y = rand() % field->getHeight();
        
        Point tilePos = {x, y};
        std::shared_ptr<Tile> tmpTile(new Tile(flowers1Surf));
        field->setTile(tmpTile, tilePos);
    }
    
    for (int i = 0; i < numTiles; ++i) {
        int x = rand() % field->getWidth();
        int y = rand() % field->getHeight();
        
        Point tilePos = {x, y};
        std::shared_ptr<Tile> tmpTile(new Tile(flowers2Surf));
        field->setTile(tmpTile, tilePos);
    }
    
    for (int i = 0; i < numTiles; ++i) {
        int x = rand() % field->getWidth();
        int y = rand() % field->getHeight();
        
        Point tilePos = {x, y};
        std::shared_ptr<Tile> tmpTile(new Tile(darkgras1Surf));
        field->setTile(tmpTile, tilePos);
    }
    
    for (int i = 0; i < numTiles; ++i) {
        int x = rand() % field->getWidth();
        int y = rand() % field->getHeight();
        
        Point tilePos = {x, y};
        std::shared_ptr<Tile> tmpTile(new Tile(darkgras2Surf));
        field->setTile(tmpTile, tilePos);
    }
    
    
    m_fields.push_back(field);
    
    // Деревья
    SDL_Rect tree1Crop = {242, 32, 118, 150};
    std::shared_ptr<Surface> tree1Surf(new Surface("tilee4.png", tree1Crop));
    std::shared_ptr<Tile> tree1Tile(new Tile(tree1Surf));
    
    SDL_Rect tree2Crop = {360, 32, 118, 150};
    std::shared_ptr<Surface> tree2Surf(new Surface("tilee4.png", tree2Crop));
    std::shared_ptr<Tile> tree2Tile(new Tile(tree2Surf));
    
    int numTrees1 = rand() % 7 + 4;
    for (int i = 0; i < numTrees1; ++i) {
        int x = rand() % (field->getWidth() * field->getTileSize() - tree1Surf->getWidth());
        int y = rand() % (field->getHeight() * field->getTileSize() - tree1Surf->getHeight());
        Point treePos = {x, y};
        
        std::shared_ptr<Tile> treeTmp(new Tile(*(tree1Tile.get())));
        treeTmp->setPosition(treePos);
        
        m_tiles.push_back(treeTmp);
    }
    
    int numTrees2 = rand() % 7 + 4;
    for (int i = 0; i < numTrees2; ++i) {
        int x = rand() % (field->getWidth() * field->getTileSize() - tree1Surf->getWidth());
        int y = rand() % (field->getHeight() * field->getTileSize() - tree1Surf->getHeight());
        Point treePos = {x, y};
        
        std::shared_ptr<Tile> treeTmp(new Tile(*(tree2Tile.get())));
        treeTmp->setPosition(treePos);
        
        m_tiles.push_back(treeTmp);
    }
    
    // Персонаж 1
    {
        SDL_Rect rect2char = {224, 0, width, height};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval tv2char1 = {2, 0};
        
        std::shared_ptr<Surface> surface2char1(new Surface("characters.png", rect2char));
        surface2char1->setColorKey(0xff00ff);
        set2char->push(surface2char1);
        times2char.push_back(tv2char1);
        
        timeval tv2char2 = {0, 200000};
        
        rect2char = {192, 0, width, height};
        
        std::shared_ptr<Surface> surface2char2(new Surface("characters.png", rect2char));
        surface2char2->setColorKey(0xff00ff);
        set2char->push(surface2char2);
        times2char.push_back(tv2char2);
        
        rect2char = {256, 0, width, height};
        
        std::shared_ptr<Surface> surface2char3(new Surface("characters.png", rect2char));
        surface2char3->setColorKey(0xff00ff);
        set2char->push(surface2char3);
        times2char.push_back(tv2char2);
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - surface2char3->getWidth());
        int y2char = rand() % (field->getHeight() * field->getTileSize() - surface2char3->getHeight());
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Персонаж 2
    {
        SDL_Rect rect2char = {320, 192, width, height};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval tv2char1 = {1, 500000};
        
        std::shared_ptr<Surface> surface2char1(new Surface("characters.png", rect2char));
        surface2char1->setColorKey(0xff00ff);
        set2char->push(surface2char1);
        times2char.push_back(tv2char1);
        
        timeval tv2char2 = {0, 300000};
        
        rect2char = {288, 192, width, height};
        
        std::shared_ptr<Surface> surface2char2(new Surface("characters.png", rect2char));
        surface2char2->setColorKey(0xff00ff);
        set2char->push(surface2char2);
        times2char.push_back(tv2char2);
        
        rect2char = {352, 192, width, height};
        
        std::shared_ptr<Surface> surface2char3(new Surface("characters.png", rect2char));
        surface2char3->setColorKey(0xff00ff);
        set2char->push(surface2char3);
        times2char.push_back(tv2char2);
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - surface2char3->getWidth());
        int y2char = rand() % (field->getHeight() * field->getTileSize() - surface2char3->getHeight());
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Персонаж 3
    {
        SDL_Rect rect2char = {128, 192, width, height};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval tv2char1 = {2, 500000};
        
        std::shared_ptr<Surface> surface2char1(new Surface("characters.png", rect2char));
        surface2char1->setColorKey(0xff00ff);
        set2char->push(surface2char1);
        times2char.push_back(tv2char1);
        
        timeval tv2char2 = {0, 150000};
        
        rect2char = {96, 192, width, height};
        
        std::shared_ptr<Surface> surface2char2(new Surface("characters.png", rect2char));
        surface2char2->setColorKey(0xff00ff);
        set2char->push(surface2char2);
        times2char.push_back(tv2char2);
        
        rect2char = {160, 192, width, height};
        
        std::shared_ptr<Surface> surface2char3(new Surface("characters.png", rect2char));
        surface2char3->setColorKey(0xff00ff);
        set2char->push(surface2char3);
        times2char.push_back(tv2char2);
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - surface2char3->getWidth());
        int y2char = rand() % (field->getHeight() * field->getTileSize() - surface2char3->getHeight());
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Персонаж 4
    {
        SDL_Rect rect2char = {320, 0, width, height};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval tv2char1 = {3, 500000};
        
        std::shared_ptr<Surface> surface2char1(new Surface("characters.png", rect2char));
        surface2char1->setColorKey(0xff00ff);
        set2char->push(surface2char1);
        times2char.push_back(tv2char1);
        
        timeval tv2char2 = {0, 150000};
        
        rect2char = {288, 0, width, height};
        
        std::shared_ptr<Surface> surface2char2(new Surface("characters.png", rect2char));
        surface2char2->setColorKey(0xff00ff);
        set2char->push(surface2char2);
        times2char.push_back(tv2char2);
        
        rect2char = {352, 0, width, height};
        
        std::shared_ptr<Surface> surface2char3(new Surface("characters.png", rect2char));
        surface2char3->setColorKey(0xff00ff);
        set2char->push(surface2char3);
        times2char.push_back(tv2char2);
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - surface2char3->getWidth());
        int y2char = rand() % (field->getHeight() * field->getTileSize() - surface2char3->getHeight());
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Призрак1
    {
        SDL_Rect rect = {0, 0, 32, 50};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval time = {0, 200000};
        
        for (int w = 0; w < 4; ++w) {
            for (int h = 0; h < 1; ++h) {
                rect.x = w * width;
                rect.y = h * height;
                std::shared_ptr<Surface> surface(new Surface("ghost.png", rect));
                surface->setAlpha(0x70);
                times2char.push_back(time);
                set2char->push(surface);
            }
        }
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - 32);
        int y2char = rand() % (field->getHeight() * field->getTileSize() - 50);
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Призрак2
    {
        SDL_Rect rect = {0, 0, 32, 50};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval time = {0, 200000};
        
        for (int w = 0; w < 4; ++w) {
            for (int h = 0; h < 1; ++h) {
                rect.x = w * width;
                rect.y = h * height;
                std::shared_ptr<Surface> surface(new Surface("ghost.png", rect));
                surface->setAlpha(0x70);
                times2char.push_back(time);
                set2char->push(surface);
            }
        }
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - 32);
        int y2char = rand() % (field->getHeight() * field->getTileSize() - 50);
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Призрак3
    {
        SDL_Rect rect = {0, 0, 32, 50};
        
        std::shared_ptr<SurfaceSet> set2char(new SurfaceSet());
        std::vector<timeval> times2char;
        timeval time = {0, 200000};
        
        for (int w = 0; w < 4; ++w) {
            for (int h = 0; h < 1; ++h) {
                rect.x = w * width;
                rect.y = h * height;
                std::shared_ptr<Surface> surface(new Surface("ghost.png", rect));
                surface->setAlpha(0x70);
                times2char.push_back(time);
                set2char->push(surface);
            }
        }
        
        std::shared_ptr<Animation> animation2char(new Animation());
        animation2char->load(set2char, times2char, ANIM_STAND_S);
        std::shared_ptr<AnimatedObject> object2char(new AnimatedObject());
        object2char->addAnimation(animation2char);
        int x2char = rand() % (field->getWidth() * field->getTileSize() - 32);
        int y2char = rand() % (field->getHeight() * field->getTileSize() - 50);
        Point pos2char = {x2char, y2char};
        object2char->setPosition(pos2char);
        object2char->runAnimation(ANIM_STAND_S, true);
        m_objects.push_back(object2char);
    }
    
    // Домики
    // Домик1
    {
        std::shared_ptr<Surface> surf(new Surface("house1.png"));
        std::shared_ptr<Tile> tile(new Tile(surf));
        
        int x = rand() % (field->getWidth() * field->getTileSize() - surf->getWidth());
        int y = rand() % (field->getHeight() * field->getTileSize() - surf->getHeight());
        Point pos = {x, y};
            
        tile->setPosition(pos);
            
        m_tiles.push_back(tile);
    }
    // Домик2
    {
        std::shared_ptr<Surface> surf(new Surface("house2.png"));
        std::shared_ptr<Tile> tile(new Tile(surf));
        
        int x = rand() % (field->getWidth() * field->getTileSize() - surf->getWidth());
        int y = rand() % (field->getHeight() * field->getTileSize() - surf->getHeight());
        Point pos = {x, y};
        
        tile->setPosition(pos);
        
        m_tiles.push_back(tile);
    }
    
    m_wasInit = true;
}

/**
 * @brief Обработчик событий
 *
 * @param event событие
 * @return void
 **/
void GameState::onEvent(SDL_Event& event) {
    if (!m_wasInit) {
        return;
    }
    
    if (event.type == SDL_KEYDOWN) {
        int speed = 4;
        
        if (event.key.keysym.sym == SDLK_RIGHT) {
            m_objects[0]->runAnimation(ANIM_WALK_E, true);
            Point coord = m_objects[0]->getPosition();
            if (coord.x + speed < static_cast<int>(m_fields[0]->getWidth() * m_fields[0]->getTileSize() - m_objects[0]->getFrame()->getWidth())) {
                coord.x += speed;
                m_objects[0]->setPosition(coord);
                std::shared_ptr<Camera> camera = Application::getSingleton().getCamera();
                Point cameraPos = camera->getPosition();
                cameraPos.x += speed;
                camera->setPosition(cameraPos);
            }
        }
        
        if (event.key.keysym.sym == SDLK_LEFT) {
            m_objects[0]->runAnimation(ANIM_WALK_W, true);
            Point coord = m_objects[0]->getPosition();
            if (coord.x - speed > 0) {
                coord.x -= speed;
                m_objects[0]->setPosition(coord);
                std::shared_ptr<Camera> camera = Application::getSingleton().getCamera();
                Point cameraPos = camera->getPosition();
                cameraPos.x -= speed;
                camera->setPosition(cameraPos);
            }
        }
        
        if (event.key.keysym.sym == SDLK_DOWN) {
            m_objects[0]->runAnimation(ANIM_WALK_S, true);
            Point coord = m_objects[0]->getPosition();
            if (coord.y + speed < static_cast<int>(m_fields[0]->getHeight() * m_fields[0]->getTileSize() - m_objects[0]->getFrame()->getHeight())) {
                coord.y += speed;
                m_objects[0]->setPosition(coord);
                std::shared_ptr<Camera> camera = Application::getSingleton().getCamera();
                Point cameraPos = camera->getPosition();
                cameraPos.y += speed;
                camera->setPosition(cameraPos);
            }
        }
        
        if (event.key.keysym.sym == SDLK_UP) {
            m_objects[0]->runAnimation(ANIM_WALK_N, true);
            Point coord = m_objects[0]->getPosition();
            if (coord.y - speed > 0) {
                coord.y -= speed;
                m_objects[0]->setPosition(coord);
                std::shared_ptr<Camera> camera = Application::getSingleton().getCamera();
                Point cameraPos = camera->getPosition();
                cameraPos.y -= speed;
                camera->setPosition(cameraPos);
            }
        }
    }
    if (event.type == SDL_KEYUP) {
        m_objects[0]->runAnimation(ANIM_STAND_S, true);
    }
}

/**
 * @brief Обработчик срабатывания таймера
 *
 * @return void
 **/
void GameState::onTimer() {
    
}
