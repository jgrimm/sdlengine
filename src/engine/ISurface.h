/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит интерфейс класса поверхности
 */

#ifndef ISURFACE_H
#define ISURFACE_H

struct Point;
class SDL_Surface;

/**
 * @brief Интерфейс класса поверхности
 **/
class ISurface {
public:
    virtual ~ISurface() {};
    
    /**
     * @brief Вернуть SDL поверхность
     *
     * @return SDL_Surface*
     **/
    virtual SDL_Surface* getSurface() const = 0;
    
    /**
     * @brief Вернуть ширину
     *
     * @return int
     **/
    virtual int getWidth() const = 0;
    
    /**
     * @brief Вернуть высоту
     *
     * @return int
     **/
    virtual int getHeight() const = 0;
    
    /**
     * @brief Вернуть признак видимости
     *
     * @return bool
     **/
    virtual bool getVisible() const = 0;
    
    /**
     * @brief Вернуть признак перерисовки
     *
     * @return bool
     **/
    virtual bool getRedraw() const = 0;
};

#endif // ISURFACE_H
