/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит интерфейс игрового поля
 */

#ifndef IFIELD_H
#define IFIELD_H

#include <memory>
#include <SDL_stdinc.h>

#include "Point.h"

class Tile;
class Surface;

/**
 * @brief Интерфейс игрового поля
 **/
class IField {
public:
    virtual ~IField() {};
    
    /**
     * @brief Вернуть тайл
     *
     * @param position положение на карте
     * @return shared_ptr<Tile>
     **/
    virtual std::shared_ptr<Tile> getTile(const Point& position) = 0;
    
    virtual std::shared_ptr<Surface> getSurface() = 0;
    
    /**
     * @brief Вернуть размер тайла
     *
     * @return Uint32
     **/
    virtual Uint32 getTileSize() const = 0;
    
    /**
     * @brief Вернуть ширину карты
     *
     * @return Uint32
     **/
    virtual Uint32 getWidth() const = 0;
    
    /**
     * @brief Вернуть высоту карты
     *
     * @return Uint32
     **/
    virtual Uint32 getHeight() const = 0;
};

#endif // IFIELD_H
