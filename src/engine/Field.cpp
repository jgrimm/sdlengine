/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса игрового поля
 */

#include <SDL_video.h>

#include "Field.h"
#include "Surface.h"
#include "Tile.h"
#include "Exception.h"

/**
 * @brief Конструктор
 *
 * @param map двумерный массив тайлов
 * @param tileSize размер тайла
 **/
Field::Field(const std::vector<std::vector<std::shared_ptr<Tile> > >& map, Uint32 tileSize) :
    m_tiles(map),
    m_tileSize(tileSize),
    m_width(0),
    m_height(0) {
    m_width = m_tiles.size();
    if (m_width > 0) {
        m_height = m_tiles.begin()->size();
    }
    
    // Выравниваем координаты координаты
    for (unsigned int x = 0; x < m_width; x++) {
        for (unsigned int y = 0; y < m_height; y++) {
            Point coord(x * tileSize, y * tileSize);
            m_tiles[x][y]->setPosition(coord);
        }
    }
}

/**
 * @brief Деструктор
 *
 **/
Field::~Field() {
}

/**
 * @brief Заменить тайл на карте
 *
 * @param tile тайл
 * @param position координаты в массиве
 * @return void
 **/
void Field::setTile(std::shared_ptr<Tile> tile, const Point& position) {
    m_tiles[position.x][position.y] = tile;
}

/**
 * @brief Вернуть тайл по координате
 *
 * @param position координаты в массиве
 * @return Tile
 **/
std::shared_ptr<Tile> Field::getTile(const Point& position) {
    return m_tiles[position.x][position.y];
}

std::shared_ptr<Surface> Field::getSurface() {
    // FIXME хранить поверхность и пересоздовать при setTile
    SDL_Rect rect = {0, 0, static_cast<Uint16>(m_width * m_tileSize), static_cast<Uint16>(m_height * m_tileSize)};
    std::shared_ptr<Surface> surface(new Surface(rect));
    
    SDL_Rect offset = {0, 0, 0, 0};
    for (Uint32 i = 0; i < m_tiles.size(); ++i) {
        for (Uint32 j = 0; j < m_tiles[0].size(); ++j) {
            offset.x = i * m_tileSize;
            offset.y = j * m_tileSize;
            surface->blit(*(m_tiles[i][j]->getSurface()), NULL, &offset);
        }
    }
    surface->setVisible(true);
    return surface;
}

/**
 * @brief Вернуть размер тайла
 *
 * @return int размер тайла в пикселях
 **/
Uint32 Field::getTileSize() const {
    return m_tileSize;
}

/**
 * @brief Вернуть ширину поля
 *
 * @return int ширина поля
 **/
Uint32 Field::getWidth() const {
    return m_width;
}

/**
 * @brief Вернуть высоту поля
 *
 * @return int высота поля
 **/
Uint32 Field::getHeight() const {
    return m_height;
}

/**
 * @brief Вернуть тип проходимости для тайла
 *
 * @param position координаты в массиве
 * @return TilePassability
 **/
TilePassability Field::getPassability(const Point& position) const {
    return m_tiles[position.x][position.y]->getPassability();
}
