/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @file
 * Этот файл содержит реализация класса фонового изображения
 */

#include "Background.h"
#include "Surface.h"

/**
 * @brief Конструктор
 *
 * @param surface фоновое изображение
 **/
Background::Background(std::shared_ptr<Surface> surface) :
    m_surface(surface) {
}

/**
 * @brief Вернуть указатель на поверхность
 *
 * @return shared_ptr< Surface >
 **/
std::shared_ptr<Surface> Background::getSurface() {
    return m_surface;
}
