/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса анимации
 */

#include <functional>

#include "Animation.h"
#include "SurfaceSet.h"
#include "TimersPool.h"

/**
 * @brief Оператор суммирования
 *
 * @param lhs левый операнд
 * @param rhs правый операнд
 * @return timeval
 **/
static timeval operator+(const timeval lhs, const timeval rhs) {
    timeval ret = {lhs.tv_sec + rhs.tv_sec, lhs.tv_usec + rhs.tv_usec};
    if(ret.tv_usec > 1000000) {
        ret.tv_usec -= 1000000;
        ++ret.tv_sec;
    }
    return ret;
}

/**
 * @brief Конструктор
 *
 **/
Animation::Animation() :
    m_type(ANIM_NONE),
    m_frames(),
    m_times(),
    m_currentFrame(0),
    m_run(false),
    m_repeat(false),
    m_timer() {
}

/**
 * @brief Деструктор
 *
 **/
Animation::~Animation() {
    stop();
}

/**
 * @brief Загрузить анимацию
 *
 * @param type тип анимации
 * @param frames набор кадров
 * @param times вектор задержек между кадрами в миллисекундах
 * 
 * @return bool
 * @retval true - успешное завершение
 * @retval false - ошибка
 **/
bool Animation::load(std::shared_ptr<SurfaceSet> frames, std::vector<timeval> times, AnimationType type) {
    if (frames->getTotalFrames() == times.size()) {
        m_type = type;
        m_frames = frames;
        m_times = times;
        m_currentFrame = 0;
        m_repeat = false;
        return true;
    }
    return false;
}

/**
 * @brief Запустить анимацию сначала
 *
 * @return void
 **/
void Animation::run(bool isRepeat) {
    if (!m_run) {
//         m_currentFrame = 0;
        m_repeat = isRepeat;
        m_run = true;
        
        std::shared_ptr<Timer> timer(new Timer(m_times[m_currentFrame], true));
        m_timer = timer;
        m_timer->setCallback(std::bind(&Animation::onTimer, this));
        TimersPool::getSingleton().addTimer(m_timer);
    }
}

/**
 * @brief Остановить анимацию
 *
 * @return void
 **/
void Animation::stop() {
    if (m_run) {
        m_run = false;
        m_timer->beginDelete();
    }
}

/**
 * @brief Получить время полного цикла анимации
 *
 * @return Uint32
 **/
timeval Animation::getTotalTime() {
    timeval total = {0, 0};
    std::vector<timeval>::iterator it = m_times.begin();
    for (; it != m_times.end(); ++it) {
        total = total + *it;
    }
    return total;
}

/**
 * @brief Получить текущий кадр анимации
 *
 * @return shared_ptr<Surface>
 **/
std::shared_ptr<Surface> Animation::getFrame() {
    return m_frames->getSurface(m_currentFrame);
}

/**
 * @brief Перемотать анимацию на начало
 *
 * @return void
 **/
void Animation::rewind() {
    m_currentFrame = 0;
}

/**
 * @brief Обработать срабатывание таймера
 *
 * @return void
 **/
void Animation::onTimer() {
    m_currentFrame++;
    if (m_currentFrame >= m_frames->getTotalFrames()) {
        m_currentFrame = 0;
        
        if (m_repeat) {
            m_run = true;
        } else {
            m_run = false;
        }
    }
    
    if (m_run) {
        std::shared_ptr<Timer> timer(new Timer(m_times[m_currentFrame], true));
        m_timer = timer;
        m_timer->setCallback(std::bind(&Animation::onTimer, this));
        TimersPool::getSingleton().addTimer(m_timer);
    }
}

/**
 * @brief Вернуть тип анимации
 *
 * @return AnimationType
 **/
AnimationType Animation::getType() {
    return m_type;
}

/**
 * @brief Проверить, запущена ли анимация
 *
 * @return bool
 **/
bool Animation::isRun() {
    return m_run;
}
