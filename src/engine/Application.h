/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса приложения
 */

#ifndef APPLICATION_H
#define APPLICATION_H

#include <memory>
#include <SDL_stdinc.h>

#include <sys/time.h>

class Camera;
class Renderer;
class GameManager;
class Timer;
union SDL_Event;

// TODO Теперь singltone. В связи с этим, переделать некоторые методы

/**
 * @brief Класс приложения. Singleton
 **/
class Application {
public:
    /**
     * @brief Получить указатель на синглтон приложения
     *
     * @return Application&
     **/
    static Application& getSingleton() {
        static Application application;
        return application;
    }

    void onInit(Uint32 width, Uint32 height, Uint32 bpp, Uint32 flags, timeval gamePeriod, timeval renderPeriod);
    void onEvent(SDL_Event& event);
    void onLoop();
    void onRender();
    void onCleanup();

    void run();
    void stop();

    std::shared_ptr<GameManager> getGameManager();
    std::shared_ptr<Camera> getCamera();
    
private:
    Application();
    virtual ~Application();
    
    Application(const Application& other);
    Application& operator=(Application& other);
    
private:
    std::shared_ptr<Camera> m_camera;               //!< Камера
    std::shared_ptr<Renderer> m_renderer;           //!< Рендерер
    std::shared_ptr<GameManager> m_gameManager;     //!< Менеджер игры
    std::shared_ptr<Timer> m_gameTimer;             //!< Таймер игрового цикла
    std::shared_ptr<Timer> m_renderTimer;           //!< Таймер отрисовки

    bool m_init;                                    //!< Флаг инициализации
    bool m_run;                                     //!< Флаг запуска
    timeval m_gamePeriod;                           //!< Период игрового цикла
    timeval m_renderPeriod;                         //!< Период отрисовки
    SDL_Event m_event;                              //!< SDL событие
};

#endif // APPLICATION_H
