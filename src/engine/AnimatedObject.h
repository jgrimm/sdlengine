/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса анимированного объекта
 */

#ifndef ANIMATED_OBJECT_H
#define ANIMATED_OBJECT_H

#include <vector>
#include <memory>
#include <SDL_stdinc.h>

#include <sys/time.h>

#include "AnimationTypes.h"
#include "Point.h"

class Surface;
class SurfaceSet;
class Animation;

// TODO переделать механизм переключения анимаций. AnimatedObject -> Sprite

/**
 * @brief Класс анимированного объекта
 **/
class AnimatedObject {
public:
    AnimatedObject();
    virtual ~AnimatedObject();

    void addAnimation(std::shared_ptr<Animation> animation);
    bool runAnimation(AnimationType type, bool isRepeat);
    void stopAnimation();

    void setPosition(const Point& coordinates);
    
    Point getPosition();
    std::shared_ptr<Surface> getFrame();
    AnimationType getAnimationType();
    timeval getAnimationTime();
    Uint32 getTotalAnimations();
    bool isAnimationRunning();

private:
    std::vector<std::shared_ptr<Animation> > m_animations;          //!< Вектор анимаций
    std::vector<std::shared_ptr<Animation> >::iterator m_current;   //!< Указатель на текущую анимацию
    Point m_position;                                               //!< Мировые координаты
};

#endif // ANIMATED_OBJECT_H
