/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса камеры
 */

#include <SDL.h>

#include "Camera.h"

/**
 * @brief Конструктор
 *
 * @param width ширина экрана
 * @param height высота экрана
 * @param bpp глубина цвета
 * @param flags флаги SDL_Surface
 **/
Camera::Camera(Uint32 width, Uint32 height, Uint32 bpp, Uint32 flags) :
    m_position(),
    m_scrn() {
    SDL_Surface* surface = SDL_SetVideoMode(width, height, bpp, flags);
    std::shared_ptr<SDL_Surface> screen(surface, SDL_FreeSurface);
    m_scrn = screen;
}

/**
 * @brief Установить координаты
 *
 * @param coordinates координаты
 * @return void
 **/
void Camera::setPosition(const Point& coordinates) {
    m_position = coordinates;
}

/**
 * @brief Вернуть координаты
 *
 * @return Point
 **/
Point Camera::getPosition() const {
    return m_position;
}

/**
 * @brief Вернуть поверхность экрана
 *
 * @return SDL_Surface
 **/
SDL_Surface* Camera::getScreen() {
    return m_scrn.get();
}

/**
 * @brief Вернуть высоту экрана
 *
 * @return int
 **/
int Camera::getHeight() const {
    return m_scrn->h;
}

/**
 * @brief Вернуть ширину экрана
 *
 * @return int
 **/
int Camera::getWidth() const {
    return m_scrn->w;
}
