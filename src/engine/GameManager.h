/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса менеджера игры
 */

#ifndef GAME_MANAGER_H
#define GAME_MANAGER_H

#include <memory>

class FSM;
union SDL_Event;

/**
 * @brief Класс менеджера игры
 **/
class GameManager {
public:
    GameManager();
    virtual ~GameManager();

    void onEvent(SDL_Event& event);
    void onTimer();
    
    void setFSM(std::shared_ptr<FSM> fsm);
    std::shared_ptr<FSM> getFSM();

private:
    std::shared_ptr<FSM> m_fsm;         //!< Конечный автомат состояний
};

#endif // GAME_MANAGER_H 
