/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит структуры для целочисленной и вещественной двумерной 
 * координаты
 */

#ifndef POINT_H
#define POINT_H

#include <cmath>

/**
 * @brief Двумерная координата
 **/
struct Point {
    int x;      //!< Координата X
    int y;      //!< Координата Y

public:
    /**
     * @brief Конструктор
     *
     **/
    Point() : x(0), y(0) {};
    
    /**
     * @brief Конструктор
     *
     * @param x_ Координата X
     * @param y_ Координата Y
     **/
    Point(int x_, int y_) : x(x_), y(y_) {};

    /**
     * @brief Оператор сравнения
     *
     * @param other другая точка
     * @return bool
     **/
    bool operator==(const Point& other) const {
        return ((x == other.x) && (y == other.y));
    };

    /**
     * @brief Расчитать расстояние до другой точки
     *
     * @param other другая точка
     * @return int
     **/
    int getDistance(const Point& other) const {
        return static_cast<int>(sqrt(static_cast<double>((x - other.x) * (x - other.x)) + static_cast<double>((y - other.y) * (y - other.y))));
    };
};

/**
 * @brief Двумерная координата
 **/
struct PointF {
    double x;      //!< Координата X
    double y;      //!< Координата Y

public:
    /**
     * @brief Конструктор
     *
     **/
    PointF() : x(0.0), y(0.0) {};
    
    /**
     * @brief Конструктор
     *
     * @param x_ Координата X
     * @param y_ Координата Y
     **/
    PointF(double x_, double y_) : x(x_), y(y_) {};

    /**
     * @brief Оператор сравнения
     *
     * @param other другая точка
     * @return bool
     **/
    bool operator==(const PointF& other) const {
        return ((x == other.x) && (y == other.y));
    };

    /**
     * @brief Расчитать расстояние до другой точки
     *
     * @param other другая точка
     * @return int
     **/
    double getDistance(const PointF& other) const {
        return sqrt((x - other.x) * (x - other.x) + (y - other.y) * (y - other.y));
    };
};

#endif // POINT_H
