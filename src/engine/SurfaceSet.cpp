/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса набора поверхностей
 */

#include <SDL.h>

#include "SurfaceSet.h"
#include "Surface.h"

/**
 * @brief Конструктор
 *
 **/
SurfaceSet::SurfaceSet() :
    m_surfaces() {
}

/**
 * @brief Деструктор
 *
 **/
SurfaceSet::~SurfaceSet() {
}

/**
 * @brief Добавить копию поверхности в стек
 *
 * @param surface поверхность
 * @return void
 **/
void SurfaceSet::push(std::shared_ptr<Surface> surface) {
    m_surfaces.push_back(surface);
}

/**
* @brief Удалить поверхность из стека
*
* @return void
**/
void SurfaceSet::pop() {
    m_surfaces.pop_back();
}

/**
 * @brief Вернуть копию поверхности из набора
 *
 * @param item номер в стеке
 * @return Surface
 **/
std::shared_ptr<Surface> SurfaceSet::getSurface(Uint32 item) const {
    if (item < m_surfaces.size()) {
        return m_surfaces.begin()[item];
    } else {
        std::shared_ptr<Surface> empty;
        return empty;
    }
}

/**
 * @brief Вернуть количество поверхностей в наборе
 *
 * @return unsigned int
 **/
Uint32 SurfaceSet::getTotalFrames() const {
    return m_surfaces.size();
}
