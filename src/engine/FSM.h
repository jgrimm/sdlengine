/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса автомата состояний
 */

#ifndef FSM_H
#define FSM_H

#include <memory>
#include <vector>
#include <SDL_stdinc.h>

class State;
union SDL_Event;

/**
 * @brief Класс автомата состояний
 **/
class FSM {
public:
    FSM();
    virtual ~FSM();

    void pushState(std::shared_ptr<State> state);
    void popState();

    void onEvent(SDL_Event& event);
    void onTImer();
    void switchState(Uint32 number);

    Uint32 getNumStates();
    std::shared_ptr<State> getCurrentState();
    Uint32 getCurrentNumber();

private:
    std::vector<std::shared_ptr<State> > m_states;     //!< Вектор состояний
    Uint32 m_current;                                  //!< Номер активного состояния
};

#endif // FSM_H 
