/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * В этом файле содержится реализация класса таймера
 */

#include <ctime>
#include <unistd.h>

#include "Timer.h"

/**
 * @brief Оператор суммирования
 *
 * @param lhs левый операнд
 * @param rhs правый операнд
 * @return timeval
 **/
static timeval operator+(const timeval lhs, const timeval rhs) {
    timeval ret = {lhs.tv_sec + rhs.tv_sec, lhs.tv_usec + rhs.tv_usec};
    if(ret.tv_usec > 1000000) {
        ret.tv_usec -= 1000000;
        ++ret.tv_sec;
    }
    return ret;
}

/**
 * @brief Оператор больше или равно
 *
 * @param lhs левый операнд
 * @param rhs правый операнд
 * @return bool
 **/
static bool operator>=(const timeval &lhs, const timeval &rhs) {
    if (lhs.tv_sec == rhs.tv_sec) {
        return (lhs.tv_usec >= rhs.tv_usec);
    }
    return (lhs.tv_sec >= rhs.tv_sec);
}

/**
 * @brief Оператор вычитания
 *
 * @param lhs левый операнд
 * @param rhs правый операнд
 * @return bool
 **/
// static timeval operator-(const timeval &lhs, const timeval &rhs) {
//     timeval ret = {lhs.tv_sec + rhs.tv_sec, lhs.tv_usec + rhs.tv_usec};
//     if(ret.tv_usec < 0) {
//         ret.tv_usec += 1000000;
//         --ret.tv_sec;
//     }
//     return ret;
// }

/**
 * @brief Конструктор
 * @param tv - время отсчета до срабатывания таймера
 * @param oneshoot - одноразовый таймер
 **/
Timer::Timer(const timeval &tv, bool oneshoot) :
    m_interval(tv),
    m_next_shoot(),
    m_oneshoot(oneshoot),
    m_func() {
    timeval shoot;
    gettimeofday(&shoot, nullptr);
    m_next_shoot = shoot + m_interval;
}

/**
 * @brief Задать функцию таймера
 *
 * @param func callback
 * @return void
 **/
void Timer::setCallback(std::function<void()> func) {
    m_func = func;
}

/**
 * @brief Корректно удалить таймер из обработчика сигнала
 *
 * Это единственный способ безопасно удалить таймер во время обработки его сигнала 
 * срабатывания
 **/
void Timer::beginDelete() {
    m_oneshoot = true;
}

/**
 * @brief Проверка срабатывания ( выполняет пул )
 *
 * В этом методе происходит вызов функции и новый взвод таймера
 * @return bool
 **/
bool Timer::check() {
    timeval shoot;
    gettimeofday(&shoot, nullptr);
    if(shoot >= m_next_shoot) {
        m_next_shoot = shoot + m_interval;
        if (m_func != nullptr) {
            m_func();
        }
        return true;
    }
    return false;
}
