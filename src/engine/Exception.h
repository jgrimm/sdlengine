/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса исключения
 */

#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>
#include <exception>

/**
 * @brief Класс исключения
 **/
class Exception : public std::exception {
public:
    Exception(const std::string& msg) throw();
    ~Exception() throw();
    
    const char* what() const throw();
    
private:
    std::string m_msg;      //!< Сообщение
};

#endif // EXCEPTION_H
