/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса игрового поля
 */

#ifndef FIELD_H
#define FIELD_H

#include <vector>
#include <memory>
#include <SDL_stdinc.h>

#include "IField.h"
#include "TileAttributes.h"
#include "Point.h"

class Tile;

//NOTE Должен содержать массив тайлов для отрисовки, карту проходимости ...
//TODO расчет пути по алгоритму А*

/**
 * @brief Класс игрового поля
 * 
 **/
class Field : public IField {
public:
    Field(const std::vector<std::vector<std::shared_ptr<Tile> > >& map, Uint32 tileSize);
    virtual ~Field();

    void setTile(std::shared_ptr<Tile> tile, const Point& position);

    virtual std::shared_ptr<Tile> getTile(const Point& position);
    virtual std::shared_ptr<Surface> getSurface();
    virtual Uint32 getTileSize() const;
    virtual Uint32 getWidth() const;
    virtual Uint32 getHeight() const;

    TilePassability getPassability(const Point& position) const;

private:
    std::vector<std::vector<std::shared_ptr<Tile> > > m_tiles;     //!< Двухмерный массив тайлов
    Uint32 m_tileSize;             //!< Размер тайла
    Uint32 m_width;                //!< Ширина поля
    Uint32 m_height;               //!< Длина поля
    // FIXME coordinates
};

#endif // FIELD_H 
