/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса, содержащего юнит-тесты для класса Camera
 */

#ifndef CAMERA_TESTS_H
#define CAMERA_TESTS_H

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>

/**
 * @brief Тестирующий класс, содержащий юнит-тесты для класса Camera
 **/
class CameraTests : public CppUnit::TestFixture {
    CPPUNIT_TEST_SUITE(CameraTests);
    CPPUNIT_TEST(setAndGetPosition_Test);
    CPPUNIT_TEST(positionAfterConstructor_Test);
    CPPUNIT_TEST_SUITE_END();
    
public:
    void setAndGetPosition_Test();
    void positionAfterConstructor_Test();
};

#endif // CAMERA_TESTS_H
