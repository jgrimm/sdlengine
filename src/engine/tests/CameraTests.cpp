/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса, содержащего юнит-тесты для класса Cameras
 */

#include <SDL.h>

#include "CameraTests.h"
#include "Camera.h"

CPPUNIT_TEST_SUITE_REGISTRATION(CameraTests);

/**
 * @brief Проверка работы методов установки и получения позиции камеры
 *
 * @return void
 **/
void CameraTests::setAndGetPosition_Test() {
    Point setPosition = {100, 300};
    Point getPosition = {0, 0};
    
    std::shared_ptr<Camera> camera(new Camera(10, 10, 32, 0));
    camera->setPosition(setPosition);
    getPosition = camera->getPosition();
    
    if ((setPosition.x != getPosition.x) || (setPosition.y != getPosition.y)) {
        CPPUNIT_FAIL("setPosition != getPosition");
    }
}

/**
 * @brief Проверка позиции камеры после создания в точке с координатами 0.0
 *
 * @return void
 **/
void CameraTests::positionAfterConstructor_Test() {
    Point getPosition = {0, 0};
    
    std::shared_ptr<Camera> camera(new Camera(10, 10, 32, 0));
    getPosition = camera->getPosition();
    
    if ((getPosition.x != 0) || (getPosition.y != 0)) {
        CPPUNIT_FAIL("start position not at 0.0!");
    }
}

