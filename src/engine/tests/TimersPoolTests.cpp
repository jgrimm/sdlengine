/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса, содержащего юнит-тесты для класса TimersPool
 */

#include <memory>

#include <unistd.h>

#include "TimersPoolTests.h"
#include "Timer.h"
#include "TimersPool.h"

CPPUNIT_TEST_SUITE_REGISTRATION(TimersPoolTests);

/**
 * @brief Проверка срабатывания одного таймера в пуле
 *
 * @return void
 **/
void TimersPoolTests::run1Timer_Test() {
    TimersPool &timersPool = TimersPool::getSingleton();
    timeval period = {0, 100};
    timersPool.setCheckPeriod(period);
    
    // Время тестирования
    timeval testTime = {0, 1000};
    std::shared_ptr<Timer> timer(new Timer(testTime));
    // Контрольная переменная
    bool checked = false;
    timer->setCallback([&checked]() {
        checked = true;
    });
    
    timersPool.addTimer(timer);
    timersPool.run();
    usleep(2 * testTime.tv_sec * 1000000 + 2 * testTime.tv_usec);

    CPPUNIT_ASSERT_EQUAL(true, checked);
}

/**
 * @brief Проверка функции остановки работы пула
 *
 * @return void
 **/
void TimersPoolTests::stopPull_Test() {
    TimersPool &timersPool = TimersPool::getSingleton();
    timeval period = {0, 100};
    timersPool.setCheckPeriod(period);

    // Время тестирования
    timeval testTime = {0, 1000};
    // Создаем циклический таймер
    std::shared_ptr<Timer> timer(new Timer(testTime));
    // Контрольная переменная
    bool checked = false;
    timer->setCallback([&checked]() {
        checked = true;
    });
    
    timersPool.run();
    // Останавливаем пул
    timersPool.stop();
    
    timersPool.addTimer(timer);
    // Ждем
    usleep(2 * testTime.tv_sec * 1000000 + 2 * testTime.tv_usec);
    
    CPPUNIT_ASSERT_EQUAL(false, checked);
}

/**
 * @brief Проверка возможности добавления таймеров в пул в процессе работы пула
 *
 * @return void
 **/
void TimersPoolTests::addTimerWhileRunning_Test() {
    TimersPool &timersPool = TimersPool::getSingleton();
    timeval period = {0, 100};
    timersPool.setCheckPeriod(period);
    
    // Время тестирования
    timeval testTime = {0, 1000};
    std::shared_ptr<Timer> timer1(new Timer(testTime, true));
    std::shared_ptr<Timer> timer2(new Timer(testTime, true));
    std::shared_ptr<Timer> timer3(new Timer(testTime, true));
    int counter = 0;
    timer1->setCallback([&counter]() {
        ++counter;
    });
    timer2->setCallback([&counter]() {
        ++counter;
    });
    timer3->setCallback([&counter]() {
        ++counter;
    });
    
    // Запускаем пул
    timersPool.run();
    // Добавляем таймеры
    timersPool.addTimer(timer1);
    timersPool.addTimer(timer2);
    timersPool.addTimer(timer3);
    
    //Ждем
    usleep(2 * testTime.tv_sec * 1000000 + 2 * testTime.tv_usec);
    
    CPPUNIT_ASSERT_EQUAL(3, counter);
}
