/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса рендерера графики
 */

#ifndef RENDERER_H
#define RENDERER_H

#include <memory>
#include <SDL_stdinc.h>

class ISurface;
class ITile;
class IField;
class Camera;
class AnimatedObject;
class Background;
class State;
class GameManager;
struct Point;

// TODO проработать все интерфейсы

/**
 * @brief Класс рендерера графики
 **/
class Renderer {
public:
    Renderer(std::shared_ptr<Camera> camera, std::shared_ptr<GameManager> gameManager);

    void drawGame();
    void drawSurface(ISurface* surface, const Point& coordinates);
    void drawTile(ITile* tile);
    void drawField(IField* field);
    void drawAnimatedObject(std::shared_ptr<AnimatedObject> object);
    void drawBackground(std::shared_ptr<Background> background);
    void drawState(std::shared_ptr<State> state);

    void cleanScreen();
    void updateScreen();

private:
    std::shared_ptr<Camera> m_camera;               //!< Камера
    std::shared_ptr<GameManager> m_gameManager;     //!< Менеджер игрового цикла
};

#endif // RENDERER_H 
