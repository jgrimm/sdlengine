/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса состояния для конечного автомата
 */

#include <SDL.h>

#include "State.h"
#include "Tile.h"
#include "Field.h"
#include "Background.h"
#include "AnimatedObject.h"
#include "Surface.h"
#include "FSM.h"

/**
 * @brief Конструктор
 *
 * @param fsm указатель на конечный автомат
 **/
State::State(std::shared_ptr<FSM> fsm) :
    m_fsm(fsm),
    m_tiles(),
    m_objects(),
    m_fields() {
}

/**
 * @brief Вернуть все тайлы состояния
 *
 * @return std::vector<std::shared_ptr<Tile> >
 **/
std::vector<std::shared_ptr<Tile> > State::getAllTiles() {
    return m_tiles;
}

/**
 * @brief Вернуть все анимированные объекты состояния
 *
 * @return std::vector<std::shared_ptr<AnimatedObject> >
 **/
std::vector<std::shared_ptr<AnimatedObject> > State::getAllObjects() {
    return m_objects;
}

/**
 * @brief Вернуть все карты состояния
 *
 * @return std::vector<std::shared_ptr<Field> >
 **/
std::vector<std::shared_ptr<Field> > State::getAllFields() {
    return m_fields;
}

/**
 * @brief Вернуть фоновое изображение
 *
 * @return std::shared_ptr< Background >
 **/
std::shared_ptr<Background> State::getBackground() {
    return m_background;
}
