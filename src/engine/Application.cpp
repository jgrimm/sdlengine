/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса приложения
 */

#include <functional>

#include <unistd.h>
#include <SDL.h>

#include "Application.h"
#include "Camera.h"
#include "Renderer.h"
#include "Timer.h"
#include "GameManager.h"
#include "FSM.h"
#include "TimersPool.h"

/**
 * @brief Конструктор
 *
 **/
Application::Application() :
    m_camera(),
    m_renderer(),
    m_gameManager(),
    m_gameTimer(),
    m_renderTimer(),
    m_init(false),
    m_run(false),
    m_gamePeriod(),
    m_renderPeriod(),
    m_event() {
    timeval game_tv = {0, 33333};           // Дефолтно 30 Гц
    m_gamePeriod = game_tv;
    timeval render_tv = {0, 16666};         // Дефолтно 60 FPS
    m_renderPeriod = render_tv;
}

/**
 * @brief Деструктор
 *
 **/
Application::~Application() {
    onCleanup();
}

/**
 * @brief Инициализация
 * 
 * @param width ширина экрана
 * @param height высота экрана
 * @param bpp глубина цвета
 * @param flags флаги SDL_Surface
 * @param gamePeriod период обновления игрового цикла
 * @param renderPeriod период обновления экрана
 * @return void
 **/
void Application::onInit(Uint32 width, Uint32 height, Uint32 bpp, Uint32 flags, timeval gamePeriod, timeval renderPeriod) {
    SDL_Init(SDL_INIT_VIDEO);
    SDL_WM_SetCaption("Project Khadgar", "Khadgar");
    
    std::shared_ptr<Camera> camera(new Camera(width, height, bpp, flags));
    m_camera = camera;
    std::shared_ptr<GameManager> gameManager(new GameManager());
    m_gameManager = gameManager;
    std::shared_ptr<Renderer> renderer(new Renderer(m_camera, m_gameManager));
    m_renderer = renderer;
    
    m_gamePeriod = gamePeriod;
    m_renderPeriod = renderPeriod;
    
    SDL_ShowCursor(SDL_DISABLE);
    
    m_init = true;
}

/**
 * @brief Обработка события
 *
 * @param event событие
 * @return void
 **/
void Application::onEvent(SDL_Event& event) {
    if (m_init) {
        m_event = event;
        
        if (event.type == SDL_QUIT) {
            stop();
        }
        
        if (event.type == SDL_KEYDOWN) {
            if (event.key.keysym.sym == SDLK_ESCAPE) {
                stop();
            }
        }
        m_gameManager->onEvent(event);
    }
}

/**
 * @brief Расчет игрового цикла
 *
 * @return void
 **/
void Application::onLoop() {
    if (m_init) {
        m_gameManager->onTimer();
    // FIXME write me
    }
}

/**
 * @brief Отрисовка игры
 *
 * @return void
 **/
void Application::onRender() {
    if (m_init) {
        m_renderer->drawGame();
        m_renderer->updateScreen();
    // FIXME write me
    }
}

/**
 * @brief Освобождение ресурсов
 *
 * @return void
 **/
void Application::onCleanup() {
    if (m_init) {
        stop();
        TimersPool::getSingleton().stop();
//         SDL_Quit();
    }
}

/**
 * @brief Запуск
 * Event pump
 *
 * @return void
 **/
void Application::run() {
    if (m_init) {
        
        // Запуск игрового цикла
        std::shared_ptr<Timer> gameTimer(new Timer(m_gamePeriod, false));
        m_gameTimer = gameTimer;
        m_gameTimer->setCallback(std::bind(&Application::onLoop, this));
//         TimersPool::getSingleton().addTimer(m_gameTimer);
        
        // Запуск рендерера
        std::shared_ptr<Timer> renderTimer(new Timer(m_renderPeriod, false));
        m_renderTimer = renderTimer;
        m_renderTimer->setCallback(std::bind(&Application::onRender, this));
//         TimersPool::getSingleton().addTimer(m_renderTimer);
        
        m_gameManager->getFSM()->switchState(0);
        
        m_run = true;
        
        TimersPool::getSingleton().run();
        
        // Обработка событий
        while (m_run) {
            SDL_PollEvent(&m_event);
            onEvent(m_event);
            
            timeval tv = m_gamePeriod;
            SDL_Delay((tv.tv_sec + tv.tv_usec) / 1000);
            
            // FIXME сделать нормально, разобраться с багами
            onLoop();
            onRender();
        }
    }
}

/**
 * @brief Остановка
 *
 * @return void
 **/
void Application::stop() {
    if (m_init) {
        m_run = false;
        m_gameTimer->beginDelete();
        m_renderTimer->beginDelete();
    }
}

/**
 * @brief Вернуть менеджер игрового процесса
 *
 * @return shared_ptr< GameManager >
 **/
std::shared_ptr<GameManager> Application::getGameManager() {
    return m_gameManager;
}

/**
 * @brief Вернуть указатель на объект камеры
 *
 * @return shared_ptr<Camera>
 **/
std::shared_ptr<Camera> Application::getCamera() {
    return m_camera;
}
