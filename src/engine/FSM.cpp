/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса автомата состояний
 */

#include <SDL_events.h>

#include "FSM.h"
#include "State.h"

/**
 * @brief Конструктор
 *
 **/
FSM::FSM() :
    m_states(),
    m_current(0) {
}

/**
 * @brief Деструктор
 *
 **/
FSM::~FSM() {
}

/**
 * @brief Добавить состояние в стек
 *
 * @param state состояние
 * @return void
 **/
void FSM::pushState(std::shared_ptr<State> state) {
    m_states.push_back(state);
}

/**
 * @brief Вытолкнуть состояние из стека
 *
 * @return void
 **/
void FSM::popState() {
    m_states.pop_back();
}

/**
 * @brief Обработать поступившее SDL событие
 *
 * @param event событие
 * @return void
 **/
void FSM::onEvent(SDL_Event& event) {
    if (m_current < m_states.size()) {
        m_states[m_current]->onEvent(event);
    }
}

/**
 * @brief Обрабочик события срабатывания таймера
 *
 * @return void
 **/
void FSM::onTImer() {
    if (m_current < m_states.size()) {
        m_states[m_current]->onTimer();
    } 
}

/**
 * @brief Переключить состояние
 *
 * @param number номер состояния
 * @return void
 **/
void FSM::switchState(Uint32 number) {
    if (number < m_states.size()) {
        m_states[m_current]->onLeave();
        m_current = number;
        m_states[m_current]->onEnter();
    }
}

/**
 * @brief Вернуть количество состояний в стеке
 *
 * @return int количество
 **/
Uint32 FSM::getNumStates() {
    return m_states.size();
}

/**
 * @brief Вернуть текущее состояние
 *
 * @return shared_ptr< State >
 **/
std::shared_ptr<State> FSM::getCurrentState() {
    if (m_current < m_states.size()) {
        return m_states[m_current];
    } else {
        std::shared_ptr<State> empty;
        return empty;
    }
}

/**
 * @brief Вернуть номер активного состояния
 *
 * @return Uint32
 **/
Uint32 FSM::getCurrentNumber() {
    return m_current;
}
