/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса менеджера игры
 */

#include "GameManager.h"
#include "FSM.h"

/**
 * @brief Конструктор
 *
 **/
GameManager::GameManager() :
    m_fsm() {
}

/**
 * @brief Деструктор
 *
 **/
GameManager::~GameManager() {
}

/**
 * @brief Обработчик события
 *
 * @param event событие
 * @return void
 **/
void GameManager::onEvent(SDL_Event& event) {
    if (m_fsm.get() != nullptr) {
        m_fsm->onEvent(event);
    }
}

/**
 * @brief Обработчик события срабатывания таймера
 *
 * @return void
 **/
void GameManager::onTimer() {
    m_fsm->onTImer();
}

/**
 * @brief Установить автомат состояний
 *
 * @param fsm автомат состояний
 * @return void
 **/
void GameManager::setFSM(std::shared_ptr<FSM> fsm) {
    m_fsm = fsm;
}

/**
 * @brief Вернуть автомат состояний
 *
 * @return std::shared_ptr< FSM >
 **/
std::shared_ptr<FSM> GameManager::getFSM() {
    return m_fsm;
}
