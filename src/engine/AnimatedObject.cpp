/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса анимированного объекта
 */

#include <SDL.h>

#include "AnimatedObject.h"
#include "Surface.h"
#include "SurfaceSet.h"
#include "Animation.h"

/**
 * @brief Конструктор
 *
 **/
AnimatedObject::AnimatedObject() :
    m_animations(),
    m_current(),
    m_position(0, 0) {
}

/**
 * @brief Деструктор
 *
 **/
AnimatedObject::~AnimatedObject() {
}

/**
 * @brief Добавить анимацию
 *
 * @param animation последовательность кадров
 * @return void
 **/
void AnimatedObject::addAnimation(std::shared_ptr<Animation> animation) {
    m_animations.push_back(animation);
    m_current = m_animations.begin();
}

/**
 * @brief Запустить анимацию
 *
 * @param type тип анимации
 * @param isRepeat признак повтора анимации
 * 
 * @return bool
 * @retval true - успешное выполнение
 * @retval false - ошибка
 **/
bool AnimatedObject::runAnimation(AnimationType type, bool isRepeat) {
    std::vector<std::shared_ptr<Animation> >::iterator it = m_animations.begin();
    for (; it != m_animations.end(); ++it) {
        if ((*it)->getType() == type) {
            (*it)->run(isRepeat);
            m_current = it;
            return true;
        }
    }
    return false;
}

/**
 * @brief Остановить анимацию
 *
 * @return bool
 * @retval true - успешное выполнение
 * @retval false - ошибка
 **/
void AnimatedObject::stopAnimation() {
    if (m_animations.size() > 0) {
        (*m_current)->stop();
    }
}

/**
 * @brief Задать координаты
 *
 * @param coordinates координаты
 * @return void
 **/
void AnimatedObject::setPosition(const Point& coordinates) {
    m_position = coordinates;
}

/**
 * @brief Получить координаты
 *
 * @return Point координаты
 **/
Point AnimatedObject::getPosition() {
    return m_position;
}

/**
 * @brief Получить кадр анимации
 *
 * @return Surface
 **/
std::shared_ptr<Surface> AnimatedObject::getFrame() {
    if (m_animations.size() > 0) {
        return (*m_current)->getFrame();
    } else {
        std::shared_ptr<Surface> empty;
        return empty;
    }
}

/**
 * @brief Получить тип активной анимации
 *
 * @return AnimationType
 **/
AnimationType AnimatedObject::getAnimationType() {
    if (m_animations.size() > 0) {
        return (*m_current)->getType();
    } else {
        return ANIM_NONE;
    }
}

/**
 * @brief Вернуть время полного цикла текущей анимации
 *
 * @return Uint32
 **/
timeval AnimatedObject::getAnimationTime() {
    if (m_animations.size() > 0) {
        return (*m_current)->getTotalTime();
    } else {
        timeval empty = {0, 0};
        return empty;
    }
}

/**
 * @brief Вернуть количество анимаций
 *
 * @return Uint32
 **/
Uint32 AnimatedObject::getTotalAnimations() {
    return m_animations.size();
}

/**
 * @brief Проверить, запущена ли анимация
 *
 * @return bool
 **/
bool AnimatedObject::isAnimationRunning() {
    if (m_animations.size() > 0) {
        return (*m_current)->isRun();
    } else {
        return false;
    }
}
