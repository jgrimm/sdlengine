/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @file 
 * В этом файле находится реализация класса пула таймеров
 */

#include <unistd.h>
#include <SDL_timer.h>

#include "TimersPool.h"

/**
 * @brief Конструктор
 *
 **/
TimersPool::TimersPool() :
    m_thread(NULL),
    m_run(false),
    m_timeval(),
    m_timers() {
    timeval tv = {0, 100000};       // Дефолтный период проверки пула 100000 мкс
    m_timeval = tv;
}

/**
 * @brief Деструктор
 *
 **/
TimersPool::~TimersPool() {
    stop();
}

/**
 * @brief Установить период проверки пула
 *
 * @param tv структура времени
 * @return void
 **/
void TimersPool::setCheckPeriod(const timeval &tv) {
    m_timeval = tv;
}

/**
 * @brief Добавить таймер в пул
 * @param timer новый таймер
 * @return void
 **/
void TimersPool::addTimer(std::shared_ptr<Timer> timer) {
    m_timers.emplace_front(timer);
}

/**
 * @brief Запустить обработку пула таймеров
 *
 * @return void
 **/
void TimersPool::run() {
    if (!m_run) {
        m_run = true;
        m_thread = SDL_CreateThread(check_thread, static_cast<void*>(this));
//         pthread_create(&m_thread, NULL, check_thread, static_cast<void*>(this));
    }
}

/**
 * @brief Остановить обработку пула таймеров
 *
 * @return void
 **/
void TimersPool::stop() {
    if (m_run) {
        m_run = false;
//         pthread_cancel(m_thread);
//         pthread_join(m_thread, NULL);
        SDL_KillThread(m_thread);
    }
}

/**
 * @brief Проверить, запущен ли поток обработки пула
 *
 * @return bool
 **/
bool TimersPool::isRun() {
    return m_run;
}

/**
 * @brief Проверка срабатывания таймеров
 **/
void TimersPool::check() {
    m_timers.remove_if([](TimerPointer &x) {
        if(x->check()) {
            // ни в одном предикате из-за порядка вычисления m_oneshoot не может быть
            // изменен внутри check!
            return x->m_oneshoot;
        }
        return false;
    });
}

/**
 * @brief Поточная функция проверки пула
 *
 * @param p_this указатель на объект
 * @return void*
 **/
int TimersPool::check_thread(void* p_this) {
//     pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
//     pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
    
    TimersPool* object = static_cast<TimersPool*>(p_this);
    
    while(object->m_run) {
        object->check();
        //select(0, NULL, NULL, NULL, &tv);
        SDL_Delay((object->m_timeval.tv_sec + object->m_timeval.tv_usec) / 1000);
    }
//     pthread_exit(NULL);
    return 0;
}
