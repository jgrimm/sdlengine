/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит различные определения для тайлов
 */

#ifndef TILE_ATTRIBUTES_H
#define TILE_ATTRIBUTES_H

/**
 * @brief Перечисление типов тайлов
 * NOTE подумать над возможными типами, составить классификацию
 **/
enum TileType {
    TILE_NOTYPE,
    TILE_SAND,
    TILE_WALL,
    TILE_GRASS,
    TILE_WATER,
    TILE_BLOCK
};

/**
 * @brief Тип проходимости тайла
 * NOTE подумать над возможными типами, составить классификацию
 **/
enum TilePassability {
    PASS_NOTYPE,
    PASS_GROUND_EASY,
    PASS_GROUND_MEDIUM,
    PASS_GROUND_HARD,
    PASS_WATER_EASY,
    PASS_WATER_MEDIUM,
    PASS_WATER_HARD,
    PASS_OBSTACLE,
    PASS_HOLE
};

#endif // TILE_ATTRIBUTES_H
