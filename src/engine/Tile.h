/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса тайла
 */

#ifndef TILE_H
#define TILE_H

#include <vector>
#include <memory>
#include <SDL_stdinc.h>

#include "ITile.h"
#include "TileAttributes.h"
#include "Point.h"

class Surface;

/**
 * @brief Класс тайла
 **/
class Tile : public ITile {
public:
    Tile(std::shared_ptr<Surface> surface);
    virtual ~Tile();

    virtual std::shared_ptr<Surface> getSurface();
    virtual Uint32 getSize() const;
    virtual Point getPosition() const;
    
    void setPosition(const Point& coordinates);
    void setType(TileType type);
    void setPassability(TilePassability pass);

    TileType getType() const;
    TilePassability getPassability() const;

private:
    Point m_position;                       //!< Экранные координаты
    TileType m_type;                        //!< Тип тайла
    TilePassability m_pass;                 //!< Тип проходимости
    std::shared_ptr<Surface> m_surface;     //!< Поверхность тайла
};

#endif // TILE_H
