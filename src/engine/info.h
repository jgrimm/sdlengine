/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @mainpage Проект 2D игрового движка, базирующегося на библиотеке SDL
 * 
 * <b><a href="https://bitbucket.org/jgrimm/sdlengine/src">Проект Khadgar</a></b>
 * 
 * Целью данного проекта является разработка 
 * кроссплатформенного игрового движка, поддерживающего работу с двухмерной 
 * графикой. Для осуществления этой задачи был выбран определенный набор 
 * технологий, помогающих в написании независимого от конкретной операционной 
 * системы программного кода. 
 * - Языком программирования выбран <b>C++</b>, в частности 
 * при кодировании активно применялись некоторые новые расширения языка из 
 * стандарта <b>C++11</b>. 
 * - Комментарии к исходным кодам составлены с использованием 
 * специализированной разметки, обрабатываемой системой автоматического 
 * документирования кода <b>Doxygen</b>.
 * - В качестве низкоуровневой подсистемы вывода графики в 
 * приложении используется свободно распространяемая библиотека <b>SDL</b>. 
 * - Для контроля целостности кода и общей истории процесса разработки 
 * выбрана система контроля версий <b>Git</b>. 
 * - Компиляция проекта осуществляется при помощи кроссплатформенной 
 * системы автоматизации сборки программного обеспечения <b>CMake</b>. 
 * - Для сборки проекта под платформы <b>GNU/Linux</b> и <b>Microsoft 
 * Windows</b> используются семейства компиляторов <b>gcc</b> и <b>mingw</b>.  
 * - Реализацию технологии юнит-тестирования обеспечивает фреймворк 
 * <b>CPPUnit</b>.
 * - Интеграционное тестирование осуществляет <b>Jenkins</b>, веб-сервис 
 * непрерывной интеграции. 
 * 
 * Дополнительная информация по 
 * некоторым используемым технологиям приведена ниже:
 * 
 * <b><a href="http://www.libsdl.org/">Simple DirectMedia Layer</a> (SDL)</b> -
 * это свободная кроссплатформенная мультимедийная библиотека, реализующая 
 * единый программный интерфейс к графической подсистеме, звуковым устройствам 
 * и средствам ввода для широкого спектра платформ. Данная библиотека является 
 * интерфейсом между приложением и используемым графическим API (OpenGL, 
 * DirectX), таким образом позволяя приложению абстрагироваться от конкретного 
 * графического стека, используемого операционной системой. SDL активно 
 * используется при написании кроссплатформенных мультимедийных программ.
 * 
 * <b><a href="http://www.cmake.org/">CMake</a> (cross platform make)</b> — 
 * это кроссплатформенная система автоматизации сборки программного обеспечения 
 * из исходного кода. CMake не занимается непосредственно сборкой, a лишь 
 * генерирует файлы управления сборкой из файлов CMakeLists.txt:
 * - Makefile в системах Unix для сборки с помощью make;
 * - файлы projects/workspaces (.dsp/.dsw) в Windows для сборки с помощью 
 * Visual C++;
 * - проекты XCode в Mac OS X.
 * 
 * <b><a href="http://www.mingw.org/">MinGW</a> (Minimalist GNU for 
 * Windows)</b>, ранее mingw32, — компилятор, нативный программный порт GNU 
 * Compiler Collection (GCC) под Microsoft Windows, вместе с набором свободно 
 * распространяемых библиотек импорта и заголовочных файлов для Windows API. 
 * MinGW позволяет разработчикам создавать нативные приложения Microsoft 
 * Windows. В MinGW включены расширения для библиотеки времени выполнения 
 * Microsoft Visual C++ для поддержки функциональности C99.
 * 
 * <b><a href="http://sourceforge.net/projects/cppunit/">CppUnit</a></b> – 
 * популярный фреймворк, являющийся портированной версией тестового фреймворка 
 * JUnit, написанного Эриком Гамма (Eric Gamma) и Кентом Беком (Kent Beck). 
 * Портированная на C++ версия была создана Майклом Фезерсом (Michael 
 * Feathers), и на ней основаны разнообразные классы, помогающие как при 
 * "тестировании белого ящика", так и при создании собственного пакета 
 * регрессионного тестирования.
 * 
 * @author Мейта Роман
 * <theshrodingerscat@gmail.com> 
 */
