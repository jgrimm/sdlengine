/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса поверхности
 */

#include "Surface.h"

#include <SDL.h>
#include <SDL_image.h>


/**
 * @brief Конструктор пустой поверхности
 *
 **/
Surface::Surface() :
    m_surface(),
    m_isVisible(false),
    m_isRedraw(false),
    m_alpha(SDL_ALPHA_OPAQUE) {
#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    Uint32 rmask = 0xff000000;
    Uint32 gmask = 0x00ff0000;
    Uint32 bmask = 0x0000ff00;
    Uint32 amask = 0x000000ff;
#else
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
#endif
    SDL_Surface* sdlSurface = SDL_CreateRGBSurface(SDL_HWSURFACE, 1, 1, 32, rmask, gmask, bmask, amask);
    SDL_Surface* sdlFormatedSurface = SDL_DisplayFormat(sdlSurface);
    SDL_FreeSurface(sdlSurface);
    std::shared_ptr<SDL_Surface> surface(sdlFormatedSurface, SDL_FreeSurface);
    m_surface = surface;
}

/**
 * @brief Конструктор пустой поверхности
 *
 * @param rectangle длина и ширина поверхности
 **/
Surface::Surface(const SDL_Rect& rectangle) :
    m_surface(),
    m_isVisible(false),
    m_isRedraw(false),
    m_alpha(SDL_ALPHA_OPAQUE) {
    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
    Uint32 rmask = 0xff000000;
    Uint32 gmask = 0x00ff0000;
    Uint32 bmask = 0x0000ff00;
    #else
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    #endif
    SDL_Surface* sdlSurface = SDL_CreateRGBSurface(SDL_HWSURFACE, rectangle.w, rectangle.h, 32, rmask, gmask, bmask, 0);
    SDL_SetColorKey(sdlSurface, SDL_SRCCOLORKEY, sdlSurface->format->colorkey);
    SDL_SetAlpha(sdlSurface, SDL_SRCALPHA, sdlSurface->format->alpha);
    SDL_Surface* sdlFormatedSurface = SDL_DisplayFormat(sdlSurface);
    SDL_FreeSurface(sdlSurface);
    std::shared_ptr<SDL_Surface> surface(sdlFormatedSurface, SDL_FreeSurface);
    m_surface = surface;
}

/**
 * @brief Конструктор поверхности из изображения
 *
 * @param fileName путь к файлу изображения
 **/
Surface::Surface(const std::string& fileName) :
    m_surface(),
    m_isVisible(true),
    m_isRedraw(false),
    m_alpha(SDL_ALPHA_OPAQUE) {
    load(fileName);
}

/**
 * @brief Конструктор поверхности из части изображения
 *
 * @param fileName путь к файлу изображения
 * @param crop область обреза исходного изображения
 **/
Surface::Surface(const std::string& fileName, SDL_Rect& crop) :
    m_surface(),
    m_isVisible(true),
    m_isRedraw(false),
    m_alpha(SDL_ALPHA_OPAQUE) {
    SDL_Surface* loadSurf = IMG_Load(fileName.c_str());
    SDL_SetColorKey(loadSurf, SDL_SRCCOLORKEY, loadSurf->format->colorkey);
    SDL_SetAlpha(loadSurf, SDL_SRCALPHA, loadSurf->format->alpha);
    
    SDL_Surface* formatedSurf = SDL_DisplayFormat(loadSurf);

//     SDL_Surface* cropedSurf = SDL_CreateRGBSurface(SDL_HWSURFACE, crop.w, crop.h, 32, 0, 0, 0, 0);

    #if SDL_BYTEORDER == SDL_BIG_ENDIAN
    Uint32 rmask = 0xff000000;
    Uint32 gmask = 0x00ff0000;
    Uint32 bmask = 0x0000ff00;
    Uint32 amask = 0x000000ff;
    #else
    Uint32 rmask = 0x000000ff;
    Uint32 gmask = 0x0000ff00;
    Uint32 bmask = 0x00ff0000;
    Uint32 amask = 0xff000000;
    #endif
    SDL_Surface* cropedSurf = SDL_CreateRGBSurface(SDL_HWSURFACE, crop.w, crop.h, 32, rmask, gmask, bmask, amask);
    SDL_Surface* cropedFormatedSurface = SDL_DisplayFormat(cropedSurf);
    SDL_FreeSurface(cropedSurf);
    
    SDL_BlitSurface(formatedSurf, &crop, cropedFormatedSurface, NULL);
    SDL_FreeSurface(formatedSurf);

    SDL_SetColorKey(cropedFormatedSurface, SDL_SRCCOLORKEY, loadSurf->format->colorkey);
    SDL_SetAlpha(cropedFormatedSurface, SDL_SRCALPHA, loadSurf->format->alpha);
    
    SDL_FreeSurface(loadSurf);
    
    std::shared_ptr<SDL_Surface> surface(cropedFormatedSurface, SDL_FreeSurface);
    m_surface = surface;
}

/**
 * @brief Конструктор копирования части изображения
 *
 * @param other поверхность-источник
 * @param crop область обреза
 **/
Surface::Surface(Surface& other, SDL_Rect& crop) :
    m_surface(),
    m_isVisible(true),
    m_isRedraw(false),
    m_alpha(SDL_ALPHA_OPAQUE) {
    SDL_Surface* sdlSurface = SDL_CreateRGBSurface(SDL_HWSURFACE, crop.w, crop.h, 32, 0, 0, 0, 0);
    std::shared_ptr<SDL_Surface> surface(sdlSurface, SDL_FreeSurface);
    m_surface = surface;
    blit(other, &crop, NULL);
}

/**
 * @brief Деструктор
 *
 **/
Surface::~Surface() {
}

/**
 * @brief Загрузка изображения
 *
 * @param fileName путь к файлу изображения
 * @return void
 **/
void Surface::load(const std::string& fileName) {
    SDL_Surface* loadSurf = IMG_Load(fileName.c_str());
    SDL_SetColorKey(loadSurf, SDL_SRCCOLORKEY, loadSurf->format->colorkey);
    SDL_SetAlpha(loadSurf, SDL_SRCALPHA, loadSurf->format->alpha);
    
    SDL_Surface* sdlSurface = SDL_DisplayFormat(loadSurf);
    SDL_FreeSurface(loadSurf);
    std::shared_ptr<SDL_Surface> surface(sdlSurface, SDL_FreeSurface);
    m_surface = surface;
}

/**
 * @brief Заливка поверхности черным цветом
 *
 * @return void
 **/
void Surface::clear() {
    if (SDL_FillRect(m_surface.get(), NULL, 0) < 0) {
//      throw cException(IMG_GetError());
    }
}

/**
 * @brief Отобразить другую поверхность на эту поверхность
 *
 * @param other другая поверхность
 * @param crop обрезающий прямоугольник
 * @param offset смещение
 * @return void
 **/
void Surface::blit(Surface& other, SDL_Rect* crop, SDL_Rect* offset) {
    SDL_BlitSurface(other.getSurface(), crop, getSurface(), offset);
}

/**
 * @brief Установить параметр видимости
 *
 * @param isVisible значение
 * @return void
 **/
void Surface::setVisible(bool isVisible) {
    m_isVisible = isVisible;
}

/**
 * @brief Установить параметр перерисовки
 *
 * @param isRedraw значение
 * @return void
 **/
void Surface::setRedraw(bool isRedraw) {
    m_isRedraw = isRedraw;
}

/**
 * @brief Установить ключевой цвет для альфа-канала
 *
 * @param color значение
 * @return void
 **/
void Surface::setColorKey(Uint32 color) {
    // FIXME пока не работает
    Uint8 r = (color & 0xFF0000) >> 16;
    Uint8 g = (color & 0x00FF00) >> 8;
    Uint8 b = (color & 0x0000FF);
    
    SDL_Surface* oldSurf = m_surface.get();
    SDL_SetColorKey(oldSurf, SDL_SRCCOLORKEY, SDL_MapRGB(m_surface->format, r, g, b));
    std::shared_ptr<SDL_Surface> formatedSurf(SDL_DisplayFormat(oldSurf));
    m_surface = formatedSurf;
}

/**
 * @brief Установить значение прозрачности
 *
 * @param alpha значение
 * @return void
 **/
void Surface::setAlpha(Uint8 alpha) {
    m_alpha = alpha;
    SDL_Surface* oldSurf = m_surface.get();
    SDL_SetColorKey(oldSurf, SDL_SRCCOLORKEY, oldSurf->format->colorkey);
    SDL_SetAlpha(oldSurf, SDL_SRCALPHA, alpha);
    std::shared_ptr<SDL_Surface> formatedSurf(SDL_DisplayFormat(oldSurf));
    m_surface = formatedSurf;
}

/**
 * @brief Вернуть SDL поверхность с текстурой
 *
 * @return SDL_Surface*
 **/
SDL_Surface* Surface::getSurface() const {
    return m_surface.get();
}

/**
 * @brief Вернуть ширину
 *
 * @return int
 **/
int Surface::getWidth() const {
    return m_surface->w;
}

/**
 * @brief Вернуть высоту
 *
 * @return int
 **/
int Surface::getHeight() const {
    return m_surface->h;
}

/**
 * @brief Вернуть параметр видимости
 *
 * @return bool
 **/
bool Surface::getVisible() const {
    return m_isVisible;
}

/**
 * @brief Вернуть параметр перерисовки
 *
 * @return bool
 **/
bool Surface::getRedraw() const {
    return m_isRedraw;
}

/**
 * @brief Получить уровень прозрачности поверхности
 *
 * @return Uint8
 **/
Uint8 Surface::getAlpha() {
    return m_alpha;
}
