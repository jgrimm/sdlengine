/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса рендерера графики
 */

#include <algorithm>
#include <SDL.h>

#include "Renderer.h"
#include "Tile.h"
#include "Field.h"
#include "Surface.h"
#include "Point.h"
#include "Camera.h"
#include "Animation.h"
#include "AnimatedObject.h"
#include "Background.h"
#include "FSM.h"
#include "State.h"
#include "GameManager.h"

/**
 * @brief Конструктор
 *
 * @param camera объект камеры
 * @param gameManager объект менеджера игры
 **/
Renderer::Renderer(std::shared_ptr<Camera> camera, std::shared_ptr<GameManager> gameManager) :
    m_camera(camera),
    m_gameManager(gameManager) {

}

/**
 * @brief Отрисовать игру
 *
 * @return void
 **/
void Renderer::drawGame() {
    cleanScreen();
    std::shared_ptr<FSM> fsm = m_gameManager->getFSM();
    if (fsm.get() != nullptr) {
        std::shared_ptr<State> state = fsm->getCurrentState();
        drawState(state);
    }
}

/**
 * @brief Отрисовать поверхность
 *
 * @param surface указатель на поверхность
 * @param coordinates координаты
 * @return void
 **/
void Renderer::drawSurface(ISurface* surface, const Point& coordinates) {
    if (surface != nullptr) {
        if (surface->getVisible()) {
            Point offset = m_camera->getPosition();
            SDL_Rect rect;
            rect.x = coordinates.x - offset.x;
            rect.y = coordinates.y - offset.y;
            // TODO проверка на выход за экран
            SDL_BlitSurface(surface->getSurface(), NULL, m_camera->getScreen(), &rect);
        }
    }
}

/**
 * @brief Отрисовать тайл
 *
 * @param tile тайл
 * @return void
 **/
void Renderer::drawTile(ITile* tile) {
    if (tile != nullptr) {
        drawSurface(tile->getSurface().get(), tile->getPosition());
    }
}

/**
 * @brief Отрисовать поле
 *
 * @param map поле
 * @return void
 **/
void Renderer::drawField(IField* map) {
    if (map != nullptr) {
//         unsigned int width = map->getWidth();
//         unsigned int height = map->getHeight();
//         
//         for (unsigned int x = 0; x < width; ++x) {
//             for (unsigned int y = 0; y < height; ++y) {
//                 Point coord(x, y);
//                 drawTile(map->getTile(coord).get());
//             }
//         }
        Point coord = {0, 0};
        drawSurface(map->getSurface().get(), coord);
    }
}
/**
 * @brief Отрисовать анимированный объект
 *
 * @param object указатель на объект
 * @return void
 **/
void Renderer::drawAnimatedObject(std::shared_ptr<AnimatedObject> object) {
    if (object.get() != nullptr) {
        drawSurface(object->getFrame().get(), object->getPosition());
    }
}

/**
 * @brief Отрисовать фон
 *
 * @param background указатель на объект фонового изображения
 * @return void
 **/
void Renderer::drawBackground(std::shared_ptr<Background> background) {
    if (background.get() != nullptr) {
        drawSurface(background->getSurface().get(), m_camera->getPosition());
    }
}

/**
 * @brief Отрисовка состояния игрового цикла
 *
 * @param state указатель на состояние
 * @return void
 **/
void Renderer::drawState(std::shared_ptr<State> state) {
    if (state.get() != nullptr) {
        std::shared_ptr<Background> background = state->getBackground();
        drawBackground(background);
        
        std::vector<std::shared_ptr<Field> > fields = state->getAllFields();
        auto it_field = fields.begin();
        for (; it_field != fields.end(); ++it_field) {
            drawField((*it_field).get());
        }
        
//         std::vector<std::shared_ptr<Tile> > tiles = state->getAllTiles();
//         std::vector<std::shared_ptr<Tile> >::iterator it_tile = tiles.begin();
//         for (; it_tile != tiles.end(); ++it_tile) {
//             drawTile((*it_tile).get());
//         }
//         
//         std::vector<std::shared_ptr<AnimatedObject> > objects = state->getAllObjects();
//         std::vector<std::shared_ptr<AnimatedObject> >::iterator it_obj = objects.begin();
//         for (; it_obj != objects.end(); ++it_obj) {
//             drawAnimatedObject(*it_obj);
//         }

        std::vector<std::shared_ptr<Tile> > tiles = state->getAllTiles();
        std::vector<std::shared_ptr<AnimatedObject> > objects = state->getAllObjects();
        auto it_obj = objects.begin();
        for (; it_obj != objects.end(); ++it_obj) {
            std::shared_ptr<Tile> tile(new Tile((*it_obj)->getFrame()));
            tile->setPosition((*it_obj)->getPosition());
            tiles.push_back(tile);
        }
        
        std::sort(tiles.begin(), tiles.end(), [](std::shared_ptr<Tile> lhs, std::shared_ptr<Tile> rhs) {
            Point plhs = lhs->getPosition();
            Uint32 slhs = lhs->getSize();
            Point prhs = rhs->getPosition();
            Uint32 rlhs = rhs->getSize();
            
            if (plhs.y + slhs < prhs.y + rlhs) {
                return true;
            }
            return false;
        });
        
        auto it_tile = tiles.begin();
        for (; it_tile != tiles.end(); ++it_tile) {
            drawTile((*it_tile).get());
        }
    }
}

/**
 * @brief Очистить экран
 *
 * @return void
 **/
void Renderer::cleanScreen() {
    SDL_FillRect(m_camera->getScreen(), NULL, 0);
}

/**
 * @brief Обновить экран
 *
 * @return void
 **/
void Renderer::updateScreen() {
    SDL_Flip(m_camera->getScreen());
}
