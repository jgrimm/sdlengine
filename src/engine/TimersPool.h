/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @file 
 * В этом файле находится описание класса пула таймеров
 */

#ifndef TIMERS_POOL_H
#define TIMERS_POOL_H

#include <memory>
#include <forward_list>
#include <SDL_thread.h>

#include <sys/time.h>

#include "Timer.h"

/**
 * @brief Класс пула таймеров. Singleton
 **/
class TimersPool {
public:
    /**
     * @brief Получить указатель на синглтон пула таймеров
     *
     * @return TimersPool&
     **/
    static TimersPool& getSingleton() {
        static TimersPool timersPool;
        return timersPool;
    }
    
    void setCheckPeriod(const timeval &tv);
    void addTimer(std::shared_ptr<Timer> timer);
    void run();
    void stop();
    bool isRun();
    
private:
    TimersPool();
    ~TimersPool();
    
    TimersPool(const TimersPool& other);
    TimersPool& operator=(TimersPool& other);
    
    void check();
    
    static int check_thread(void* p_this);
    
private:
    typedef std::shared_ptr<Timer> TimerPointer;        //!< Умный указатель на таймер
    
    SDL_Thread *m_thread;                               //!< Указатель на дескриптор потока
    bool m_run;                                         //!< Признак запуска
    timeval m_timeval;                                  //!< Частота проверки пула
    std::forward_list<TimerPointer> m_timers;           //!< Все таймеры пула
};

#endif // TIMERS_POOL_H
