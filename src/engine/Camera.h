/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса камеры
 */

#ifndef CAMERA_H
#define CAMERA_H

#include <memory>
#include <SDL_stdinc.h>

#include "Point.h"

class SDL_Surface;

/**
 * @brief Класс камеры
 **/
class Camera {
public:
    Camera(Uint32 width, Uint32 height, Uint32 bpp, Uint32 flags);

    void setPosition(const Point& coordinates);

    Point getPosition() const;
    SDL_Surface* getScreen();

    int getWidth() const;
    int getHeight() const;
    
private:
    Point m_position;                           //!< Мировые координаты
    std::shared_ptr<SDL_Surface> m_scrn;        //!< Поверхность экрана
};

#endif // CAMERA_H
