/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса поверхности
 */

#ifndef SURFACE_H
#define SURFACE_H

#include <string>
#include <memory>
#include <SDL_stdinc.h>

#include "ISurface.h"

class SDL_Rect;

/**
 * @brief Класс поверхности
 **/
class Surface : public ISurface {
public:
    Surface();
    Surface(const SDL_Rect& rectangle);
    Surface(const std::string& fileName);
    Surface(const std::string& fileName, SDL_Rect& crop);
    Surface(Surface& other, SDL_Rect& crop);
    virtual ~Surface();

    void load(const std::string& fileName);
    //void load(const std::string& fileName, SDL_Rect& crop);
    void clear();
    void blit(Surface& other, SDL_Rect* crop, SDL_Rect* offset);
    void setVisible(bool isVisible);
    void setRedraw(bool isRedraw);
    void setColorKey(Uint32 color);
    void setAlpha(Uint8 alpha);

    virtual SDL_Surface* getSurface() const;
    virtual int getWidth() const;
    virtual int getHeight() const;
    virtual bool getVisible() const;
    virtual bool getRedraw() const;
    Uint8 getAlpha();

private:
    std::shared_ptr<SDL_Surface> m_surface;     //!< SDL поверхность
    bool m_isVisible;                           //!< Видимость
    bool m_isRedraw;                            //!< Необходимость обновления
    Uint8 m_alpha;                              //!< Значение прозрачности
};

#endif // SURFACE_H
