/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*!
 * @file
 * Этот файл содержит описание класса фонового изображения
 */

#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <memory>

class Surface;

// TODO проработать класс

/**
 * @brief Класс фонового изображения
 **/
class Background {
public:
    Background(std::shared_ptr<Surface> surface);
    
    std::shared_ptr<Surface> getSurface();
    
private:
    std::shared_ptr<Surface> m_surface;
};

#endif // BACKGROUND_H
