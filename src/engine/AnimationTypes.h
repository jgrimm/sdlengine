/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит перечисление типов анимаций
 */

#ifndef ANIMATED_TYPES_H
#define ANIMATED_TYPES_H

/**
 * @brief Перечисление типов анимаций
 **/
enum AnimationType_t {
    ANIM_NONE,
    ANIM_STAND_N,
    ANIM_STAND_NE,
    ANIM_STAND_E,
    ANIM_STAND_SE,
    ANIM_STAND_S,
    ANIM_STAND_SW,
    ANIM_STAND_W,
    ANIM_STAND_WN,
    ANIM_WALK_N,
    ANIM_WALK_NE,
    ANIM_WALK_E,
    ANIM_WALK_SE,
    ANIM_WALK_S,
    ANIM_WALK_SW,
    ANIM_WALK_W,
    ANIM_WALK_WN,
    ANIM_ATACK_N,
    ANIM_ATACK_NE,
    ANIM_ATACK_E,
    ANIM_ATACK_SE,
    ANIM_ATACK_S,
    ANIM_ATACK_SW,
    ANIM_ATACK_W,
    ANIM_ATACK_WN,
    ANIM_DEATH_N,
    ANIM_DEATH_NE,
    ANIM_DEATH_E,
    ANIM_DEATH_SE,
    ANIM_DEATH_S,
    ANIM_DEATH_SW,
    ANIM_DEATH_W,
    ANIM_DEATH_WN
};
/**
 * @brief typedef для AnimationType_t
 **/
typedef AnimationType_t AnimationType;

#endif // ANIMATED_TYPES_H
