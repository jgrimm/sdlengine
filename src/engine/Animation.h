/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса анимации
 */

#ifndef ANIMATION_H
#define ANIMATION_H

#include <vector>
#include <memory>
#include <SDL_stdinc.h>

#include <sys/time.h>

#include "Timer.h"
#include "AnimationTypes.h"

class Surface;
class SurfaceSet;

// TODO сделать отдельный класс Frame, содержащий, в том числе, время кадра

/**
 * @brief Класс анимации
 **/
class Animation {
public:
    Animation();
    virtual ~Animation();

    bool load(std::shared_ptr<SurfaceSet> frames, std::vector<timeval> times, AnimationType type);
    void run(bool isRepeat);
    void stop();

    timeval getTotalTime();
    std::shared_ptr<Surface> getFrame();
    AnimationType getType();
    bool isRun();

private:
    void rewind();
    void onTimer();

private:
    AnimationType m_type;                       //!< Тип анимации
    std::shared_ptr<SurfaceSet> m_frames;       //!< Последовательность кадров
    std::vector<timeval> m_times;               //!< Вектор задержек между кадрами
    Uint32 m_currentFrame;                      //!< Текущий кадр
    bool m_run;                                 //!< Признак воспроизведения
    bool m_repeat;                              //!< Признак циклического повтора анимации
    std::shared_ptr<Timer> m_timer;             //!< Внутренний таймер
};

#endif // ANIMATION_H
