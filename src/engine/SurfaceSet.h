/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса набора поверхностей
 */

#ifndef SURFACE_SET_H
#define SURFACE_SET_H

#include <vector>
#include <memory>
#include <SDL_stdinc.h>

#include "Point.h"

class Surface;

// TODO переработать методы класса

/**
 * @brief Класс набора поверхностей
 **/
class SurfaceSet {
public:
    SurfaceSet();
    virtual ~SurfaceSet();

    void push(std::shared_ptr<Surface> surface);
    void pop();

    std::shared_ptr<Surface> getSurface(Uint32 item) const;
    Uint32 getTotalFrames() const;

private:
    std::vector<std::shared_ptr<Surface> > m_surfaces;    //!< Стек поверхностей
};

#endif // SURFACE_SET_H
