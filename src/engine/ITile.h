/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит интерфейс класса тайла
 */

#ifndef ITILE_H
#define ITILE_H

#include <memory>
#include <SDL_stdinc.h>

class Surface;
class Point;

/**
 * @brief Интерфейс тайла
 **/
class ITile {
public:
    virtual ~ITile() {};
    
    /**
     * @brief Вернуть поверхность
     *
     * @return shared_ptr<Surface>
     **/
    virtual std::shared_ptr<Surface> getSurface() = 0;
    
    /**
     * @brief Вернуть размер тайла
     *
     * @return Uint32
     **/
    virtual Uint32 getSize() const = 0;
    
    /**
     * @brief Вернуть координаты
     *
     * @return Point
     **/
    virtual Point getPosition() const = 0;
};

#endif // ITILE_H 
