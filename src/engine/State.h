/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит описание класса состояния для конечного автомата
 */

#ifndef STATE_H
#define STATE_H

#include <memory>
#include <vector>

class Tile;
class Field;
class AnimatedObject;
class Background;
class FSM;
class Surface;
union SDL_Event;

/**
 * @brief Абстрактный класс состояния для конечного автомата
 **/
class State {
public:
    State(std::shared_ptr<FSM> fsm);
    virtual ~State() {};

    /**
     * @brief Входа в состояние
     *
     * @return void
     **/
    virtual void onEnter() = 0;
    
    /**
     * @brief Выхода из состояния
     *
     * @return void
     **/
    virtual void onLeave() = 0;
    
    /**
     * @brief Загрузка ресурсов
     *
     * @return void
     **/
    virtual void onLoad() = 0;
    
    /**
     * @brief Обработчик событий
     *
     * @param event событие
     * @return void
     **/
    virtual void onEvent(SDL_Event& event) = 0;
    
    /**
     * @brief Обработчик срабатывания таймера
     *
     * @return void
     **/
    virtual void onTimer() = 0;

    std::vector<std::shared_ptr<Tile> > getAllTiles();
    std::vector<std::shared_ptr<AnimatedObject> > getAllObjects();
    std::vector<std::shared_ptr<Field> > getAllFields();
    std::shared_ptr<Background> getBackground();

protected:
    std::shared_ptr<FSM> m_fsm;                                 //!< Указатель на конечный автомат состояний
    std::vector<std::shared_ptr<Tile> > m_tiles;                //!< Вектор тайлов
    std::vector<std::shared_ptr<AnimatedObject> > m_objects;    //!< Вектор анимированных объектов
    std::vector<std::shared_ptr<Field> > m_fields;              //!< Вектор тайловых полей
    std::shared_ptr<Background> m_background;                   //!< Фоновое изображение
};

#endif // STATE_H
