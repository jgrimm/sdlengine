/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * В этом файле содержится описание класса таймера
 */

#ifndef TIMER_H
#define TIMER_H

#include <functional>
#include <pthread.h>

#include <sys/time.h>

/**
 * @brief Класс таймера
 **/
class Timer {
public:
    Timer(const timeval &tv, bool oneshoot = false);

    void setCallback(std::function<void()> func);
    void beginDelete();

protected:
    friend class TimersPool;
    
    bool check();
    
protected:
    timeval m_interval;             //!< Интервал срабатывания
    timeval m_next_shoot;           //!< Время последнего срабатывания
    bool m_oneshoot;                //!< Признак одноразового таймера
    
    std::function<void()> m_func;   //!< Функция, вызываемая таймером
};

#endif // TIMER_H
