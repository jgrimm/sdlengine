/*
 *  Project Khadgar. 2D Game Engine based on SDL library
 *  Copyright (C) 2013  Meyta Roman <theshrodingerscat@gmail.com>
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*! @file
 * Этот файл содержит реализацию класса тайла
 */

#include <SDL_image.h>

#include "Tile.h"
#include "Surface.h"

/**
 * @brief Конструктор
 *
 * @param surface текстура тайла
 **/
Tile::Tile(std::shared_ptr<Surface> surface) :
    m_position(),
    m_type(TILE_NOTYPE),
    m_pass(PASS_NOTYPE),
    m_surface(surface) {
}

/**
 * @brief Деструктор
 *
 **/
Tile::~Tile() {
}

/**
 * @brief Вернуть поверхность тайла
 *
 * @return shared_ptr<Surface> поверхность
 **/
std::shared_ptr<Surface> Tile::getSurface() {
    return m_surface;
}

/**
 * @brief Вернуть размер тайла в пикселях
 * Возвращается значение ширины базовой текстуры
 *
 * @return int значение
 **/
unsigned int Tile::getSize() const {
    if (m_surface->getHeight() > m_surface->getWidth()) {
        return m_surface->getHeight();
    }
    return m_surface->getWidth();
}

/**
 * @brief Установить позицию тайла
 *
 * @param coordinates экранные координаты
 * @return void
 **/
void Tile::setPosition(const Point& coordinates) {
    m_position = coordinates;
}

/**
 * @brief Установить тип тайла
 *
 * @param type тип
 * @return void
 **/
void Tile::setType(TileType type) {
    m_type = type;
}

/**
 * @brief Установить тип проходимости
 *
 * @param pass тип проходимости
 * @return void
 **/
void Tile::setPassability(TilePassability pass) {
    m_pass = pass;
}

/**
 * @brief Вернуть координаты
 *
 * @return int
 **/
Point Tile::getPosition() const {
    return m_position;
}

/**
 * @brief Вернуть тип тайла
 *
 * @return TileType
 **/
TileType Tile::getType() const {
    return m_type;
}

/**
 * @brief Вернуть проходимость тайла
 *
 * @return TilePassability
 **/
TilePassability Tile::getPassability() const {
    return m_pass;
}
